function varargout=plotExportedSpike(W,Gauss,Xwin,varargin)

% Plots PSTH and raster from the structure made by 'exportSpikeAndTraces.m'.
% Input
%   -W:     Window size (msec) for calculating PSTH.  When using Gaussian
%           window function, it corresponds to the SD.
%   -Gauss: If using Gaussian window function, 1 (default).  If 0, it uses
%           sliding window overlapping 50%.
%   -Xwin:  2-element vector to define the xlim of the plots
%   -varargin:  structure(s) made by 'exportSpikeAndTraces.m'.
%
%Output
%   Optionally outputs calculated PSTH data as a structure.
%   Each field is odor and contains matrix; each row corresonds to the input structure.
%   


if isempty(W);W=50;end
if isempty(Gauss);Gauss=1;end

fname=fieldnames(varargin{1});

if Gauss
    Tres=min([10,W/5]);%time resolution of psth (msec)
else
    Tres=W/2;
end
T=0:Tres/1000:varargin{1}.(fname{1}).Time(end);
if isempty(Xwin);Xwin=[0,max(T)];end
for f=1:length(fname)
    PSTH{f}=zeros(length(varargin),length(T));
end
for i=1:length(varargin)
    Data=varargin{i};
    for f=1:length(fname)
        S=Data.(fname{f}).Spike;
        if Gauss
            PSTH{f}(i,:)=calcPSTHgauss(T,W,S);
        else
            PSTH{f}(i,:)=calcPSTHslide(T,W,S);
        end
        Spike{i,f}=S;
    end
end

figure('color','w')
for i=1:length(fname)
    subplot(2,length(fname),i)
    plot(T,PSTH{i})
    title(fname{i})
    if nargout>0
        varargout{1}.(fname{i})=PSTH{i};
    end
end
for i=1:length(fname)
    subplot(2,length(fname),i+length(fname))
    plotRaster(Spike(:,i))
end
set(findobj(gcf,'type','axes'),'xlim',Xwin)
end

function PSTH=calcPSTHgauss(T,W,S)
Spikes=[];
PSTH=zeros(size(T));
for i=1:length(S)
    if ~isempty(S{i})
        Spikes=[Spikes,S{i}];
    end
end
if ~isempty(Spikes)
    for i=1:length(PSTH)
        Prob=normpdf(Spikes,T(i),W/1000);
        PSTH(i)=sum(Prob);
    end
end
PSTH=PSTH/length(S);
end

function PSTH=calcPSTHslide(T,W,S)
Spikes=[];
PSTH=zeros(size(T));
for i=1:length(S)
    if ~isempty(S{i})
        Spikes=[Spikes,S{i}];
    end
end
if ~isempty(Spikes)
    for j=1:length(PSTH)
        PSTH(j)=sum(sum(Spikes>=T(j)-W/2/1000 ...
            & Spikes<T(j)+W/2/1000));
    end
end
PSTH=PSTH/W*1000/length(S);
end

function plotRaster(S)
barlength=0.8; %Must be <1
xrange=[0 15]; %x range to plot
linewidth=1; %width of raster
seplinecolor=[.2 .2 .2]; %color of separate lines
seplinewidth=1; %linewidth of separate line

DATA=cat(1,S{:});%Combined data from all odor
rasterX = [];
rasterY = [];

for i=1:size(S,1)
    invSize(i)=length(S{size(S,1)-i+1,1});
end

for i=1:length(invSize)-1
    linelocation(i)=sum(invSize(1:i))+0.5;
end

labellocation=(invSize+1)/2;
for i=2:length(invSize)
    labellocation(i)=(sum(invSize(1:i-1))+sum(invSize(1:i))+1)/2;
end

for j=1:length(DATA)
    n=3*length(DATA{j});
    x=NaN(n,1);
    y=x;
    x(1:3:n)=DATA{j}';
    x(2:3:n)=DATA{j}';
    y(1:3:n)=length(DATA)-j+1+barlength/2;
    y(2:3:n)=length(DATA)-j+1-barlength/2;
    
    rasterX=[rasterX;x];
    rasterY=[rasterY;y];
end
plot(rasterX,rasterY,'k-','linewidth',linewidth)
set(gca,'Xlim',xrange,...
    'Ylim',[0 j+1],'YTick',[],...
    'YTickLabel',[],'Tickdir','out','box','off','linewidth',1)

for i=1:length(linelocation)
    line(xrange,[linelocation(i),linelocation(i)],'linewidth',seplinewidth,...
        'color',seplinecolor)
end

% for i=1:length(labellocation)
%     text(xrange(1),labellocation(i),SortedSpikeCell{end-i+1,1},...
%         'Rotation',90,...
%         'HorizontalAlignment','center',...
%         'VerticalAlignment','bottom');
% end
end
