function Data=psthDAcell(DAcell,START,END,BIN,axesdim)

%This function plots psth using 'DAcell' (please refer 'rasterDAcell.m' about DAcell)
%Each odor is subplotted in new figure, and psths from before, during after
%DA are overlayed.
%
%Input:
%DAcell
%START:start point to create bins (secs)
%END:end point of bins (secs)
%BIN:bin size (msec)
%
%Output
%Data:structure that contains binned data
%Data.count has the same dimension as input DAcell, but now the data is the
%matrices containing spike count of each bin. Each row is trial, and each
%column is bin.
%Data.mean is matrix that contains mean spike rate of 3 conditions (before,
%during, and after DA). Each column is 1 condition.
%Data.time is time bin(sec)
%Data.bin is binsize

if nargin<5
    axesdim=ceil(sqrt(size(DAcell,1)))*[1,1];
end
odorcolor=[1 1 .6]; %color of patch
odortime=[3 4]; %odor presentation time
xrange=[0 10]; %x range to plot
yrange=[0 150]; %y range to plot
linewidth=1; %linewidth

Data=binDAcell(DAcell,START,END,BIN);

figure

for i=1:size(DAcell,1)
    subplot(axesdim(1),axesdim(2),i)
    plot(Data.time,Data.mean{i,2})
    set(gca,'Xlim',xrange,'Ylim',yrange,'linewidth',linewidth,'box','off')
    patch([odortime(1);odortime(2);odortime(2);odortime(1)],...
        [yrange(1);yrange(1);yrange(2);yrange(2)],[-1;-1;-1;-1],...
        odorcolor,'EdgeColor','none');
    title(Data.mean{i,1})
end

end

function OUT=binDAcell(DAcell,START,END,BIN)

OUT.count=cell(size(DAcell));
OUT.count(:,1)=DAcell(:,1);
OUT.mean=cell(size(DAcell,1),2);
OUT.mean(:,1)=DAcell(:,1);

for i=1:size(DAcell,1)
    for k=2:size(DAcell,2)
        tempmat=zeros(size(DAcell{i,k},1),length(START:BIN/1000:END)-1);
        for j=1:size(tempmat,1)
            tempvec=histc(DAcell{i,k}{j},START:BIN/1000:END);
            tempmat(j,:)=tempvec(1:end-1);
        end
        OUT.count{i,k}=tempmat;
        OUT.mean{i,2}(:,k-1)=mean(tempmat,1)'/BIN*1000;
    end
end

OUT.time=START+BIN/1000/2:BIN/1000:END-BIN/1000/2;
OUT.bin=BIN;
end