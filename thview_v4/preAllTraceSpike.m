spikedeletewarn=questdlg('Spike data will be reset!','Delete spikes?','GO!','Cancel','Cancel');
switch spikedeletewarn
    case 'GO!'
        ...
    otherwise
        return
end

waitbarh=waitbar(0,'Detecting spikes...',...
    'CreateCancelBtn','delete(waitbarh)');

for j=1:numsweep
    clear Data_*
    clear Spike_*
    clear preSpike_*
    eval(['load ' loadfile ' Data_' int2str(j)])
    eval(['currentsweep=Data_' int2str(j) ';'])
    switch currentsweep.amplifier2.Mode
        case 'I-Clamp'

            %Copied from 'SingleTraceSpike'
            [b,a]=butter(round(spikedetpara.Order/2),...
                [spikedetpara.Low spikedetpara.High]/(currentsweep.parameter.ai_sr/2));
            origV=double(currentsweep.data.voltage2*1000/currentsweep.amplifier2.Vgain); %filtfilt doesn't work properly for single
            filtV = filtfilt(b,a,origV);
            currentprespike=zeros(0,1); %[time,V]
            spikepos=[];

            % DEFINE  DETECTION THRESHOLD RELATIVE TO THE NOISE
            Threshold=spikedetpara.Threshold*std(filtV);

            % FIND THE ACTUAL CROSSING POINTS OF THE THRESHOLD
            rr = 1:numel(filtV)-1 ;
            poscross=find(filtV(rr)<Threshold & filtV(rr+1)>=Threshold);
            negcross=find(filtV(rr)>=Threshold & filtV(rr+1)<Threshold);

            % Take the max bet. positive & negative crossing points
            % That's the spike!
            if ~isempty(poscross) && ~isempty(negcross)
                negcross=negcross(negcross>min(poscross));
                for i=1:min([length(poscross),length(negcross)]);
                    spikepos(i)=poscross(i)-1+find(origV(poscross(i):negcross(i))...
                        ==max(origV(poscross(i):negcross(i))),1,'first');
                end
                currentprespike=[currentsweep.data.time(spikepos),...
                    currentsweep.data.voltage2(spikepos)*1000/currentsweep.amplifier2.Vgain];
                % Remove the spikes below minimum absolute value
                currentprespike(currentprespike(:,2)<spikedetpara.Min,:)=[];
            end

            % Save in the spike file
            eval(['preSpike_' int2str(j) '=currentprespike;'])
            if exist(spikefile,'file')
                eval(['save ' spikefile ' preSpike_' int2str(j) ' -append'])
            else
                eval(['save ' spikefile ' preSpike_' int2str(j)])
            end


            % Update the spike list
            if isempty(strmatch(sprintf('preSpike_%d',j),prespikelist,'exact'))
                prespikelist=[prespikelist;sprintf('preSpike_%d',j)];
            end
    end
    waitbar(j/numsweep,waitbarh)
    if ~ishandle(waitbarh)
        break
    end
end

lbxfcn

if ishandle(waitbarh)
    delete(waitbarh)
end