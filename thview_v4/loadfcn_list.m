%[FileName,PathName]=uigetfile('D:\Data\Janelia\Patch\*mat','Select Data File');

if cell_n > 1
    FileName = cell_list(cell_n, 2);
    FileName = FileName{1, 1};
    FileName = [FileName, '.mat'];
    PathName = cell_list(cell_n, 1);
    PathName = PathName{1, 1};
    PathName = [PathName, '\'];
else
end


if FileName~=0
    set(lbxh,'Value',1);
    set(lbxh,'String',{});
    clear Data_*
    filenum=[];
    set(tableRh,'Visible','off')

    set(fileh,'String',FileName(1:(length(FileName)-4)))
    loadfile=[PathName FileName];
    spikefile=[PathName FileName(1:(length(FileName)-4)) '_spike.mat'];
    selectionfile=[PathName FileName(1:(length(FileName)-4)) '_selection.mat'];
    waitbarh=waitbar(0,'Acquiring file information...',...
        'CreateCancelBtn','delete(waitbarh)');
    sweeplist=who('Data_*','-file',loadfile);
    numsweep=size(sweeplist,1);

    spikelist={'some string'};
    if exist(spikefile,'file')
        spikelist=who('Spike_*','-file',spikefile);
    end
    prespikelist={'some string'};
    if exist(spikefile,'file')
        prespikelist=who('preSpike_*','-file',spikefile);
    end
    if exist(selectionfile,'file')
        eval(['load ' selectionfile ' rejectedsweeps'])
    else
        rejectedsweeps=zeros(numsweep,1);
        eval(['save ' selectionfile ' rejectedsweeps'])
    end

    pause(0.01)
    if ishandle(waitbarh)
    else
        set(fileh,'String','')
        return
    end


    filelist=cell(numsweep,1);
    waitbar(0,waitbarh,'Loading data...',...
        'CreateCancelBtn','delete(waitbarh)');
    for i=1:numsweep
        eval(['load ' loadfile ' Data_' int2str(i) ';'])
        eval(['currentsweep=Data_' int2str(i) ';'])

        if isfield(currentsweep,'odor')
            if currentsweep.stim
                filelist(i)={[int2str(i) '_' currentsweep.menuname '_' int2str(currentsweep.menunum)...
                    '_' currentsweep.odor '_Stim']};
            else
                filelist(i)={[int2str(i) '_' currentsweep.menuname '_' int2str(currentsweep.menunum)...
                    '_' currentsweep.odor]};
            end
        elseif isfield(currentsweep,'Color')
            filelist(i)={[int2str(i),'_',...
                currentsweep.menuname,'_',int2str(currentsweep.menunum),...
                '_',currentsweep.Color,'_',int2str(currentsweep.Intensity)]};
        else
            filelist(i)={[int2str(i) '_' currentsweep.menuname '_' int2str(currentsweep.menunum)]};
        end

        clear Data_* currentsweep
        waitbar(i/numsweep,waitbarh)
        if ishandle(waitbarh)
        else
            break
        end

    end

    if ishandle(waitbarh)
        set(lbxh,'string',filelist)
        delete(waitbarh)
    else
        set(lbxh,'string',{})
        set(fileh,'String','')
    end

    % if str2num(FileName(1:6))<90515
    %     set(lbxh,'Callback','lbxfcn_old3')
    % else
    %     set(lbxh,'Callback','lbxfcn')
    % end

end

set(celln_disph, 'String', ['Cell number ' int2str(cell_n) ' of ' int2str(n_cells)] );