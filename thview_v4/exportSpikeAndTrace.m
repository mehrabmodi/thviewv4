function Out=exportSpikeAndTrace
%Exports spike data, raw traces of selected trials in thview3
%
%
multifilenum=evalin('base','multifilenum');
rejectedsweeps=evalin('base','rejectedsweeps');
loadfile=evalin('base','loadfile');
spikefile=evalin('base','spikefile');

for i=1:length(multifilenum)
    
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        clear Spike_*
        nospike=0;
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
        try
            eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
            eval(['currentspike=Spike_' int2str(multifilenum(i)) ';'])
        catch %when there's no spike data
            nospike=1;
        end
        if isfield(currentsweep,'odor')
            thisodor=currentsweep.odor;
        else
            thisodor='NoOdor';
        end
        switch currentsweep.amplifier.Mode
            case 'I-Clamp'
                currenttrace=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;
            case 'V-Clamp'
                currenttrace=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
        end
        if ~nospike            
            if isempty(currentspike)
                currentspike=single(zeros(0,1));
            end
            try
                Out.(thisodor).Spike{end+1,1}=currentspike(:,1)';
            catch
                Out.(thisodor).Spike={currentspike(:,1)'};
            end
        end
        try
            Out.(thisodor).Trace(end+1,:)=currenttrace';
        catch
            Out.(thisodor).Trace=currenttrace';
            Out.(thisodor).Time=currentsweep.data.time';
        end
        try
            Out.(thisodor).Trial(end+1)=multifilenum(i);
        catch
            Out.(thisodor).Trial=multifilenum(i);
        end
    end
end