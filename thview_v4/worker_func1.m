%This function is called for spike detection and re-plotting V-trace when
%thresh-adjust sliders are used.

%checking if a cell has been loaded
if exist('rejectedsweeps', 'var') == 0
    disp('load a cell and try again')
    
else

    %plotting Voltage trace
    plotwhatdata{1} = 'voltage';    %telling Toshi's function what to plot
    lbxfcn;                         %calling Toshi's plotting function

    if strcmp(algtype, 'KC') == 1
        %calling Glenn's spike detection algorithm
        thresh1_val = spikedetpara.Thresh1;
        thresh2_val = spikedetpara.Thresh2;
        thresh3_val = spikedetpara.Thresh3;
        V_trace = double(currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain);
        V_trace_plot = double(currentsweep.data.voltage);
        sf = currentsweep.parameter.ai_sr;
        time_vec = 1/sf:1/sf:(length(V_trace)./sf);

        spike_i = Glenn_DetectSpikes11_m_20160811(V_trace, 30, 300, thresh1_val, thresh2_val, 0, thresh3_val);
        
        currentspike=[currentsweep.data.time(spike_i),...
                    currentsweep.data.voltage(spike_i)*1000/currentsweep.amplifier.Vgain];

     %% Toshi's code to save detected spikes to file, plot them.
        %Save in the spike file
        eval(['Spike_' int2str(filenum) '=currentspike;'])
        if exist(spikefile,'file')
            eval(['save ' spikefile ' Spike_' int2str(filenum) ' -append'])
        else
            eval(['save ' spikefile ' Spike_' int2str(filenum)])
        end


        % Update the spike list
        if isempty(strmatch(sprintf('Spike_%d',filenum),spikelist,'exact'))
            spikelist=[spikelist;sprintf('Spike_%d',filenum)];
        end

        % Update the plot
        lbxfcn

    elseif strcmp(algtype, 'Other') == 1
        %Calling Toshi's spike detection algorithm
        SingleTraceSpike;
    else
    end
    
end