%This function is called for spike detection and re-plotting V-trace when
%thresh-adjust sliders are used.


%plotting Voltage trace
plotwhatdata{1} = 'voltage';    %telling Toshi's function what to plot
lbxfcn;                         %calling Toshi's plotting function

if exist('algtype', 'var') == 0
        algtype = 'KC';
        set(r_KCalgh, 'Value', 1)
else
end

if strcmp(algtype, 'KC') == 1
    %calling Glenn's spike detection algorithm
    thresh1_val = spikedetpara.Thresh1;
    thresh2_val = spikedetpara.Thresh2;
    thresh3_val = spikedetpara.Thresh3;
    V_trace = double(currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain);

    spike_i = Glenn_DetectSpikes11_m_20160811(V_trace, 30, 300, thresh1_val, thresh2_val, 0, thresh3_val);
    currentspike=[currentsweep.data.time(spike_i),...
                   currentsweep.data.voltage(spike_i)*1000/currentsweep.amplifier.Vgain;];
                
    %Save in the spike file
    eval(['Spike_' int2str(filenum) '=currentspike;'])
   
    if exist(spikefile,'file') == 2
        eval(['save ' spikefile ' Spike_' int2str(filenum) ' -append'])
    else
        eval(['save ' spikefile ' Spike_' int2str(filenum)])
    end
    
    
    % Update the spike list
    if isempty(strmatch(sprintf('Spike_%d',filenum),spikelist,'exact'))
        spikelist=[spikelist;sprintf('Spike_%d',filenum)];
    else
    end
    
    
elseif strcmp(algtype, 'Other') == 1
     SingleTraceSpike_m;
     
else
end

%Update the plot
lbxfcn
    
                