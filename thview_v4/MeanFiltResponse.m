function MeanFiltResponse
load D:\Data_TH\Summary\FilteredResponse.mat
L=who;
[SelectedVars,OK]=listdlg('ListString',L,...
                           'PromptString','Select data',...
                           'Name','Data selection',...
                           'InitialValue',[]);
if isempty(SelectedVars)
   return
end
M=[];
for i=1:length(SelectedVars)
    eval(['M(end+1,:)=' L{SelectedVars(i)} '.odormean;'])
end
eval(['T=' L{1} '.time;'])
figure;
plot(T,M,'color',.5*ones(1,3))
hold on
plot(T,mean(M,1),'r','linewidth',2)