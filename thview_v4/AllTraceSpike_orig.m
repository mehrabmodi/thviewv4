spikedeletewarn=questdlg('Spike data will be reset!','Delete spikes?','GO!','Cancel','Cancel');
switch spikedeletewarn
    case 'GO!'
        ...
    otherwise
    return
end

waitbarh=waitbar(0,'Detecting spikes...',...
    'CreateCancelBtn','delete(waitbarh)');

for j=1:numsweep
    clear Data_*
    clear Spike_*
    eval(['load ' loadfile ' Data_' int2str(j)])
    eval(['currentsweep=Data_' int2str(j) ';'])
    switch currentsweep.amplifier.Mode
        case 'I-Clamp'
            
            %Copied from 'SingleTraceSpike'
            [b,a]=butter(round(spikedetpara.Order/2),...
                [spikedetpara.Low spikedetpara.High]/(currentsweep.parameter.ai_sr/2));
            origV=double(currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain); %filtfilt doesn't work properly for single
            filtV = filtfilt(b,a,origV);
            currentspike=zeros(0,1); %[time,V]
            spikepos=[];
            
            % DEFINE  DETECTION THRESHOLD RELATIVE TO THE NOISE
            Threshold=spikedetpara.Threshold*std(filtV);
            
            % FIND THE ACTUAL CROSSING POINTS OF THE THRESHOLD
            rr = 1:numel(filtV)-1 ;
            poscross=find(filtV(rr)<Threshold & filtV(rr+1)>=Threshold);
            negcross=find(filtV(rr)>=Threshold & filtV(rr+1)<Threshold);
            
            % Take the max bet. positive & negative crossing points
            % That's the spike!
            if ~isempty(poscross) && ~isempty(negcross)
                negcross=negcross(negcross>min(poscross));
                for i=1:min([length(poscross),length(negcross)]);
                    spikepos(i)=poscross(i)-1+find(origV(poscross(i):negcross(i))...
                        ==max(origV(poscross(i):negcross(i))),1,'first');
                end
                currentspike=[currentsweep.data.time(spikepos),...
                    currentsweep.data.voltage(spikepos)*1000/currentsweep.amplifier.Vgain];
                % Remove the spikes below minimum absolute value
                currentspike(currentspike(:,2)<spikedetpara.Min,:)=[];
            end
        case 'V-Clamp'
            [b,a]=butter(round(spikedetpara.Order/2),...
                [spikedetpara.Low spikedetpara.High]/(currentsweep.parameter.ai_sr/2));
            origV=-double(currentsweep.data.current*1000/currentsweep.amplifier.Igain); %filtfilt doesn't work properly for single
            filtV = filtfilt(b,a,origV);
            currentspike=zeros(0,1); %[time,V]
            spikepos=[];
            
            % DEFINE  DETECTION THRESHOLD RELATIVE TO THE NOISE
            Threshold=spikedetpara.Threshold*std(filtV);
            
            % FIND THE ACTUAL CROSSING POINTS OF THE THRESHOLD
            rr = 1:numel(filtV)-1 ;
            poscross=find(filtV(rr)<Threshold & filtV(rr+1)>=Threshold);
            negcross=find(filtV(rr)>=Threshold & filtV(rr+1)<Threshold);
            
            % Take the max bet. positive & negative crossing points
            % That's the spike!
            if ~isempty(poscross) && ~isempty(negcross)
                negcross=negcross(negcross>min(poscross));
                for i=1:min([length(poscross),length(negcross)]);
                    spikepos(i)=poscross(i)-1+find(origV(poscross(i):negcross(i))...
                        ==max(origV(poscross(i):negcross(i))),1,'first');
                end
                currentspike=[currentsweep.data.time(spikepos),...
                    currentsweep.data.current(spikepos)*1000/currentsweep.amplifier.Igain];
                % Remove the spikes below minimum absolute value
                %currentspike(currentspike(:,2)<spikedetpara.Min,:)=[];
            end
    end
    
    % Save in the spike file
    eval(['Spike_' int2str(j) '=currentspike;'])
    if exist(spikefile,'file')
        eval(['save ' spikefile ' Spike_' int2str(j) ' -append'])
    else
        eval(['save ' spikefile ' Spike_' int2str(j)])
    end
    
    
    % Update the spike list
    if isempty(strmatch(sprintf('Spike_%d',j),spikelist,'exact'))
        spikelist=[spikelist;sprintf('Spike_%d',j)];
    end
    
    waitbar(j/numsweep,waitbarh)
    if ~ishandle(waitbarh)
        break
    end
end


lbxfcn

if ishandle(waitbarh)
    delete(waitbarh)
end