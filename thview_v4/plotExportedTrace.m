function varargout=plotExportedTrace(Xwin,varargin)

fname=fieldnames(varargin{1});
T=varargin{1}.(fname{1}).Time;
if isempty(Xwin);Xwin=[0,max(T)];end
for f=1:length(fname)
    M{f}=zeros(length(varargin),length(T));
end
figure('color','w');
Y=[];
for i=1:length(varargin)
    Data=varargin{i};
    for f=1:length(fname)
        M{f}(i,:)=mean(Data.(fname{f}).Trace,1);
        subplot(length(varargin)+1,length(fname),f+(i-1)*length(fname))
        plot(T,Data.(fname{f}).Trace,'color',.7*ones(1,3))
        hold on
        plot(T,M{f}(i,:),'r')
        axis tight
        Y(end+1,:)=ylim;
    end
end

for f=1:length(fname)
    subplot(length(varargin)+1,length(fname),f+length(varargin)*length(fname))
    plot(T,M{f})
    if nargout>0
        varargout{1}.(fname{f})=M{f};
    end
end
set(findobj(gcf,'type','axes'),'xlim',Xwin,'ylim',[min(Y(:,1)),max(Y(:,2))])