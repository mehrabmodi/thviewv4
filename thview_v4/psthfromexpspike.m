function out=psthfromexpspike(varargin)

binsize=200; %bin (msec)
odorcolor=[1 1 .6]; %color of patch
odortime=[3 4]; %odor presentation time
xrange=[1 7]; %x range to plot
yrange=[0 80]; %y range to plot
linewidth=1.5; %line width

time=[xrange(1):binsize/2/1000:xrange(2)]';
out=time;
for i=1:length(varargin)
    spikerate=zeros(size(time));
    
    for j=1:length(spikerate)
        spikerate(j)=sum(sum(varargin{i}>=time(j)-binsize/2/1000 ...
                               & varargin{i}<time(j)+binsize/2/1000)...
                                /binsize*1000)/size(varargin{i},2);
    end
    out=[out,spikerate];
end

%figure('position',[0 495 940 439]);
plot(time,out(:,2:end),'linewidth',linewidth)
set(gca,'Xlim',xrange,'Ylim',yrange,'box','off')
patch([odortime(1);odortime(2);odortime(2);odortime(1)],...
      [yrange(1);yrange(1);yrange(2);yrange(2)],[-1;-1;-1;-1],...
      odorcolor,'EdgeColor','none');