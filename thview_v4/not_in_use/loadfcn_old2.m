[FileName,PathName]=uigetfile('C:\Data_TH\*mat','Select Data File');

if FileName==0
else
set(lbxh,'Value',1); 
set(lbxh,'String',{});
clear Data_*
set(parawinh,'Visible','off')

set(fileh,'String',FileName(1:(length(FileName)-4)))
loadfile=[PathName FileName];
waitbarh=waitbar(0,'Acquiring file information...',...
    'CreateCancelBtn','delete(waitbarh)');
sweeplist=who('Data_*','-file',loadfile);
pause(0.01)
if ishandle(waitbarh)
else
    set(fileh,'String','')
    return
end
numsweep=size(sweeplist,1);

filelist=cell(numsweep,1);
waitbar(0,waitbarh,'Loading data...',...
    'CreateCancelBtn','delete(waitbarh)');
for i=1:numsweep
    eval(['load ' loadfile ' Data_' num2str(i) ';'])
    eval(['currentsweep=Data_' num2str(i) ';'])
    filelist(i)={[num2str(i) '_' currentsweep.menu '_' num2str(currentsweep.menunum)]};
    clear Data_* currentsweep
    waitbar(i/numsweep,waitbarh)
    if ishandle(waitbarh)
    else
        break
    end
    
end

if ishandle(waitbarh)
    set(lbxh,'string',filelist)
    delete(waitbarh)
else
    set(lbxh,'string',{})
    set(fileh,'String','')
end

end