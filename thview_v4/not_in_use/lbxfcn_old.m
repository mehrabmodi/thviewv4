filenum=get(lbxh,'Value');
eval(['currentsweep=Data_' num2str(filenum) ';'])

set(parawinh,'Visible','off')
switch currentsweep.menu
    case 'menu1'
        set(ai_sr_1h,'String',num2str(currentsweep.parameter(1).ai_sr))
        set(ao_sr_1h,'String',num2str(currentsweep.parameter(1).ao_sr))
        set(dur_1h,'String',num2str(currentsweep.parameter(1).dur))
        set(numt_1h,'String',num2str(currentsweep.parameter(1).numt))
        set(iti_1h,'String',num2str(currentsweep.parameter(1).iti))
        set(pre_1h,'String',num2str(currentsweep.parameter(1).pre))
        set(shockN_1h,'String',num2str(currentsweep.parameter(1).shockN))
        set(isi_1h,'String',num2str(currentsweep.parameter(1).isi))
        
        set(menu1h,'Visible','on')
        
    case 'menu2'
        set(ai_sr_2h,'String',num2str(currentsweep.parameter(2).ai_sr))
        set(ao_sr_2h,'String',num2str(currentsweep.parameter(2).ao_sr))
        set(dur_2h,'String',num2str(currentsweep.parameter(2).dur))
        set(numt_2h,'String',num2str(currentsweep.parameter(2).numt))
        set(iti_2h,'String',num2str(currentsweep.parameter(2).iti))
        set(pre_2h,'String',num2str(currentsweep.parameter(2).pre))
        set(shockN_2h,'String',num2str(currentsweep.parameter(2).shockN))
        set(shockD_2h,'String',num2str(currentsweep.parameter(2).shockD))
        set(isi_2h,'String',num2str(currentsweep.parameter(2).isi))
        
        set(menu2h,'Visible','on')
end

          
plot(ploth,currentsweep.data(:,1),currentsweep.data(:,3));
grid on
set(acqtimeh,'String',currentsweep.time)