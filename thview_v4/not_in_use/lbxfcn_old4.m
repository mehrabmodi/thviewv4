
if length(get(lbxh,'Value'))==1
    if get(overlayh,'value')
        set(findobj(anawinh,'type','axes'),'visible','off')
        set(ploth,'visible','on')
        
        previouscolor=get(findobj(anawinh,'type','line'),'color');
        if iscell(previouscolor)
            previouscolor=cell2mat(get(findobj(anawinh,'type','line'),'color'));
        end
            
        previouscolor(previouscolor>0 & previouscolor~=0.8)=1;
        previouscolor(previouscolor==0)=0.8;
        previouscolor=mat2cell(previouscolor,ones(1,size(previouscolor,1)),3);
        set(findobj(anawinh,'type','line'),{'color'},previouscolor)
        hold on
    end
    set(anawinh,'CurrentAxes',ploth)
    filenum=get(lbxh,'Value');
    multifilenum=filenum;

    if rejectedsweeps(filenum)
        set(rejectstatush,'visible','on')
        set(rejecth,'string','Accept')
    else
        set(rejectstatush,'visible','off')
        set(rejecth,'string','Reject')
    end

    clear Data_*
    clear Spike_*
    eval(['load ' loadfile ' Data_' int2str(filenum) ';'])
    eval(['currentsweep=Data_' int2str(filenum) ';'])
    if ~isempty(strmatch(sprintf('Spike_%d',filenum),spikelist,'exact'))
        eval(['load ' spikefile ' Spike_' int2str(filenum) ';'])
        eval(['currentspike=Spike_' int2str(filenum) ';'])
    else
        currentspike=[];
    end

    set(tableRh,'data',currentsweep.paratable(:,3:4))
    set(tableRh,'rowname',currentsweep.paratable(:,2))
    set(tableRh,'visible','on')
    % if isfield(currentsweep,'clampmode')
    % else currentsweep.clampmode='voltage';%To accept old files without 'clampmode' field
    % end
    switch currentsweep.amplifier.Mode
        case 'I-Clamp'
            if isfield(currentsweep,'motion') && get(showmotionh,'value')
                plotyy(currentsweep.data.time,...
                currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain,...
                currentsweep.motion.time,currentsweep.motion.cc)
            else
                plot(ploth,currentsweep.data.time,...
                currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain);
            end
            set(Ylabelh,'string','mV')
        case 'V-Clamp'
            if get(zerosubh,'value')
                currentsweep.data.current=currentsweep.data.current-mean(currentsweep.data.current);
            end
            if isfield(currentsweep,'motion')
            plotyy(ploth,currentsweep.data.time,...
                currentsweep.data.current*1000/currentsweep.amplifier.Igain,...
                currentsweep.motion.time,currentsweep.motion.cc)
            else
                plot(ploth,currentsweep.data.time,...
                currentsweep.data.current*1000/currentsweep.amplifier.Igain);
            end
            set(Ylabelh,'string','pA')
    end

    if get(overlayh,'value') && get(showmotionh,'value')
        set(findobj(anawinh,'type','line','-not','parent',ploth),'parent',...
            findobj(anawinh,'type','axes','YAxisLocation','right','visible','on'))
        delete(findobj(anawinh,'type','axes','visible','off'))
        axis(findobj(anawinh,'type','axes'),'auto')
    end

    if ~isempty(currentspike)&& get(showspikeh,'value')
        hold on
        plot(ploth,currentspike(:,1),currentspike(:,2),'r+')
    end
    grid on
    
    if get(fixXscaleh,'value')
        set(ploth,'xlim',XLIM)
    end

    if get(fixYscaleh,'value')
        set(ploth,'ylim',YLIM)
    end
    hold off
    set(acqtimeh,'String',currentsweep.time)
else
    multifilenum=get(lbxh,'Value');
end