if length(get(lbxh,'Value'))==1
multifilenum=1;
filenum=get(lbxh,'Value');
clear Data_*
eval(['load ' loadfile ' Data_' int2str(filenum) ';'])
eval(['currentsweep=Data_' int2str(filenum) ';'])

set(tableRh,'data',currentsweep.paratable(:,3:4))
set(tableRh,'rowname',currentsweep.paratable(:,2))
set(tableRh,'visible','on')
if isfield(currentsweep,'clampmode')
else currentsweep.clampmode='voltage';%To accept old files without 'clampmode' field
end
    switch currentsweep.clampmode
        case 'voltage'
          if isfield(currentsweep,'motion')
               plotyy(currentsweep.data(:,1),currentsweep.data(:,3),...
               currentsweep.motion(:,1),currentsweep.motion(:,2))
          else
               plot(ploth,currentsweep.data(:,1),currentsweep.data(:,3));
          end 
        case 'current'
          if isfield(currentsweep,'motion')
               plotyy(currentsweep.data(:,1),currentsweep.data(:,2),...
               currentsweep.motion(:,1),currentsweep.motion(:,2))
          else
               plot(ploth,currentsweep.data(:,1),currentsweep.data(:,2));
          end 
    end

grid on
set(acqtimeh,'String',currentsweep.time)
else
multifilenum=get(lbxh,'Value');
end