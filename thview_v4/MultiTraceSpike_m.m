spikedeletewarn=questdlg('Spike data will be reset!','Delete spikes?','GO!','Cancel','Cancel');
switch spikedeletewarn
    case 'GO!'
        ...
    otherwise
    return
end
waitbarh=waitbar(0,'Detecting spikes...',...
    'CreateCancelBtn','delete(waitbarh)');


if exist('algtype', 'var') == 0
        algtype = 'KC';
        set(r_KCalgh, 'Value', 1)
else
end


for k=1:length(multifilenum)
    j=multifilenum(k);
    clear Data_*
    clear Spike_*
    eval(['load ' loadfile ' Data_' int2str(j)])
    eval(['currentsweep=Data_' int2str(j) ';'])
    keyboard
    %calling the appropriate spike detection algorithm
    if strcmp(algtype, 'KC') == 1
        %Calling Glenn's algorithm
        thresh1_val = spikedetpara.Thresh1;
        thresh2_val = spikedetpara.Thresh2;
        thresh3_val = spikedetpara.Thresh3;
        V_trace = double(currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain);

        spike_i = Glenn_DetectSpikes11_m_20160811(V_trace, 30, 300, thresh1_val, thresh2_val, 0, thresh3_val);
        currentspike=[currentsweep.data.time(spike_i),...
                       currentsweep.data.voltage(spike_i)*1000/currentsweep.amplifier.Vgain;];

        %Save in the spike file
        eval(['Spike_' int2str(filenum) '=currentspike;'])

        if exist(spikefile,'file') == 2
            eval(['save ' spikefile ' Spike_' int2str(filenum) ' -append'])
        else
            eval(['save ' spikefile ' Spike_' int2str(filenum)])
        end


        % Update the spike list
        if isempty(strmatch(sprintf('Spike_%d',filenum),spikelist,'exact'))
            spikelist=[spikelist;sprintf('Spike_%d',filenum)];
        else
        end

               
    elseif strcmp(algtype, 'Other') == 1
        %Calling Toshi's algorithm
        SingleTraceSpike_m;
        
    else
    end
    
    
    waitbar(k/length(multifilenum),waitbarh)
    if ~ishandle(waitbarh)
        break
    end
    
    keyboard
end

%Update the plot
lbxfcn

if ishandle(waitbarh)
    delete(waitbarh)
end