function plotallPSTHDAdata(DAdata)

N=inputname(1);
N=N(7:end);
data=DAdata.PSTH200;
Time=data.time;
R=data.mean;
R(strcmpi('air',R(:,1)),:)=[];
R(strcmpi('iaa_3',R(:,1)),:)=[];
R(strcmpi('iaa_4',R(:,1)),:)=[];
R(strcmpi('iaa_5',R(:,1)),:)=[];

h=figure('color','w','position',[15,320,1250,610],...
    'name',N);

for i=1:size(R,1)
    subplot(2,3,i)
    plot(Time, R{i,2}(:,1:2),'linewidth',1.5)
    set(gca,'linewidth',2)
    title(R{i,1})
    box off
end

eval(['saveas(h,''D:\Data_TH\DApsth\' N 'psth.fig'')'])