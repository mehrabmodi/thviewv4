function LD_princomp3(range,bin)
%Perform LDA using first 'numPC' PC.
%'range' is epoch of interest...[START,END] in sec
%'bin' is binsize (ms)
%number of PC will be optimized

classf = @(XTRAIN, ytrain,XTEST)(classify(XTEST,XTRAIN,ytrain));

[INPUT,VarName]=selectfromws;
if isempty(INPUT)
    return
end

numdata=length(INPUT);
correct=zeros(numdata,2);
for j=1:numdata
    data=INPUT{j};
    data=SpikeCountBin(data,range(1),range(2),bin);
    data(end,:)=[];
    %data(end,:)=[]; %uncomment if you want to remove air responses
    group={};
    mintrial=[];
    for k=1:size(data,1)
        group(end+1:end+size(data{k,2},1))=data(k,1);
        mintrial(k)=size(data{k,2},1);
    end
    mintrial=min(mintrial);
    group=group';
    randgroup=group(randperm(length(group))');
    cp=cvpartition(group,'leaveout');
    randcp=cvpartition(randgroup,'leaveout');
    all=cell2mat(data(:,2));
    [~, Pall]=princomp(all);
    tempcorrect=[];
    for m=1:min(size(Pall,2),mintrial-1)
    P=Pall(:,1:m);
    MCR=crossval('mcr',P,group,'predfun',classf,'partition',cp);
    tempcorrect(m)=1-MCR;
    end
    [maxcorrect,optnum]=max(tempcorrect);
    P=Pall(:,1:optnum);
    randMCR=crossval('mcr',P,randgroup,'predfun',classf,'partition',randcp);
    correct(j,:)=[maxcorrect,1-randMCR];
    
end

figure
boxplot(correct,{'Data';'Randomized'})
hold on
line(get(gca,'xlim'),1/size(data,1)*[1 1],'color','r','linewidth',1,'linestyle',':')

X=[(rand(size(correct,1),1)-0.5)*0.1+1,(rand(size(correct,1),1)-0.5)*0.1+2];
scatter(X(:,1),correct(:,1),[],'r','filled')
scatter(X(:,2),correct(:,2),[],'r','filled')

set(gca,'ylim',[0,1])