function [Icorrect,Ccorrect,Itime,Ctime]=PWLD_rawdata(range,step,bin)

%Perform piece-wise discrimination using instantaneous or expanding range spike rate.

classf = @(XTRAIN, ytrain,XTEST)(classify(XTEST,XTRAIN,ytrain));

[INPUT,VarName]=selectfromws;
if isempty(INPUT)
    return
end

numdata=length(INPUT);
Icorrect=zeros(2,length(range(1):step/1000:range(2)-bin/1000),numdata);
Ccorrect=Icorrect;
Itime=zeros(1,length(range(1):step/1000:range(2)-bin/1000));
Ctime=Itime;

figure

for j=1:numdata
    data=INPUT{j};
    START=range(1);
    
    for i=1:length(Itime)
        Idata=SpikeCountBin(data,START,START+bin/1000,bin);
        Idata(end,:)=[];
        %data(end,:)=[]; %uncomment if you want to remove air responses
        if i==1
            group={};
            for k=1:size(Idata,1)
                group(end+1:end+size(Idata{k,2},1))=Idata(k,1);
            end
            group=group';
            randgroup=group(randperm(length(group))');
        end
        Cdata=SpikeCountBin(data,range(1),START+bin/1000,(START-range(1))*1000+bin);
        Cdata(end,:)=[];
        cp=cvpartition(group,'leaveout');
        randcp=cvpartition(randgroup,'leaveout');
        Idata=cell2mat(Idata(:,2));
        Cdata=cell2mat(Cdata(:,2));
        IMCR=crossval('mcr',Idata,group,'predfun',classf,'partition',cp);
        randIMCR=crossval('mcr',Idata,randgroup,'predfun',classf,'partition',randcp);
        CMCR=crossval('mcr',Cdata,group,'predfun',classf,'partition',cp);
        randCMCR=crossval('mcr',Cdata,randgroup,'predfun',classf,'partition',randcp);
        Icorrect(:,i,j)=[1-IMCR;1-randIMCR];
        Ccorrect(:,i,j)=[1-CMCR;1-randCMCR];
        if j==1;
            Itime(i)=START+bin/2000;
            Ctime(i)=START+bin/1000;
        end
        START=START+step/1000;
    end
    subplot(1,numdata,j)
    hold on
    plot(Itime,Icorrect(2,:,j),'k:')
    plot(Itime,Icorrect(1,:,j),'k-')
    plot(Ctime,Ccorrect(2,:,j),'r:')
    plot(Ctime,Ccorrect(1,:,j),'r-')
    line(get(gca,'xlim'),1/size(Cdata,1)*[1 1],'color','g','linewidth',1,'linestyle',':')
end


