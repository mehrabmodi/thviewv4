currentvar=who;

[AppendFile,AppendPath]=uigetfile('E:\Data_TH\*mat','Select File to Add Data');

if AppendFile==0
    return
end

AppendFile=[AppendPath,AppendFile];
existingvar=who('-file',AppendFile);

[SelectedVars,OK]=listdlg('ListString',currentvar,...
                           'PromptString','Select variables to save',...
                           'Name','Variable selection',...
                           'InitialValue',[]);
if isempty(SelectedVars)
    return
end

YESTOALL=0;
savedvars={};
for i=1:length(SelectedVars)
    if YESTOALL
        eval(['save ' AppendFile ' ' currentvar{SelectedVars(i)} ' -append'])
        savedvars{end+1}=currentvar{SelectedVars(i)};
    else
        alreadythere=strmatch(currentvar{SelectedVars(i)},existingvar,'exact');
        if isempty(alreadythere)
            eval(['save ' AppendFile ' ' currentvar{SelectedVars(i)} ' -append'])
            savedvars{end+1}=currentvar{SelectedVars(i)};
        else            
            button=questdlg(sprintf('%s already exists. Do you want to replace it?',currentvar{SelectedVars(i)}),...
                'Replace variable?','Yes','Yes to All','No','No');
            switch button
                case 'Yes'
                    eval(['save ' AppendFile ' ' currentvar{SelectedVars(i)} ' -append'])
                    savedvars{end+1}=currentvar{SelectedVars(i)};
                case 'Yes to All'
                    eval(['save ' AppendFile ' ' currentvar{SelectedVars(i)} ' -append'])
                    savedvars{end+1}=currentvar{SelectedVars(i)};
                    YESTOALL=1;
            end
        end
    end
end

disp('Saved following variables')
savedvars

clear AppendFile AppendPath OK SelectedVars SelelctedVars YESTOALL alreadythere button currentvar existingvar i savedvars
