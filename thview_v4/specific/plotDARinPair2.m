function Data=plotDARinPair2(Concentration,newfig)

minspont=0; %minimum spontaneous fireing rate
ppos=[1,2.5];%position of ttest text
if nargin<2
    newfig=1;
end
D=[];

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end

for i=1:length(INPUT)
    data=INPUT{i};
    if data.reject || ~isfield(data,'Rin') || isempty(data.Rin) || data.DAconc~=Concentration  || data.SpontRate(1)<minspont
        continue
    end
    D=[D;data.Rin(1:2)];
end

Data.data=D;
[h,p]=ttest(Data.data(:,1),Data.data(:,2));
Data.p=p;

if p>0.05
    ptext=sprintf('\\itp\\rm=%.2f',p);
elseif p>0.01
    ptext='* \itp\rm<0.05';
elseif p>0.001
    ptext='** \itp\rm<0.01';
else
    ptext='*** \itp\rm<0.001';
end

if newfig
    figure
    set(gcf,'color','w')
end

plot(Data.data(:,1),Data.data(:,2),'o',...
    'color','k',...
    'markersize',18,...
    'linewidth',1.5,...
    'MarkerFaceColor','w')
box off
axis square
XL=get(gca,'xlim');
YL=get(gca,'ylim');
L=[min([XL,YL]),max([XL,YL])];
set(gca,'xlim',L,...
    'ylim',L)
line(L,L,'linewidth',1)

ylabel('R_{input}  [Dopamine]  (M\Omega)','fontsize',14)
xlabel('R_{input}  [Before]  (M\Omega)','fontsize',14)
set(gca,'linewidth',2,'fontsize',12)

text(ppos(1),ppos(2),sprintf('%s (\\itn\\rm=%d)',ptext,size(Data.data,1)),...
    'fontsize',15,...
    'Horizontal','left',...
    'Vertical','top')
