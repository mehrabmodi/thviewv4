function addOdorPatch(OdorTime,Color,Ylim,Mergin)
if nargin<4 || isempty(Mergin)
    Mergin=1;
end
if nargin<3 || isempty(Ylim)
    Ylim=get(gca,'YLim');
end
if nargin<2 || isempty(Color)
    Color=[1 1 .6];
end
if nargin<1 || isempty(OdorTime)
    OdorTime=[3,4];
end

Y=Ylim(2)-Ylim(1);
Ylim=[Ylim(1)+Y*Mergin/100,Ylim(2)-Y*Mergin/100];

NextPlot=get(gca,'NextPlot');
YLIM=get(gca,'YLim');
hold on

patch([OdorTime(1);OdorTime(2);OdorTime(2);OdorTime(1)],...
    [Ylim(1);Ylim(1);Ylim(2);Ylim(2)],[-1;-1;-1;-1],...
    Color,'EdgeColor','none');
set(gca,'NextPlot',NextPlot,'Ylim',YLIM)