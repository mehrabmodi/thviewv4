function restrate=restrateDAcell(DAcell,Window)

%Calculate resting firing rate from 'DAcell'.
%Refer to 'rasterDScell.m' about DAcell.
%It uses the time window defined by 'Window' (sec) of all the trials.
%Output is 3 elements vector which corresponds to the firing rate (Hz) of 3
%conditions; before, during, and after DA

for i=2:size(DAcell,2)
    data=cat(1,DAcell{:,i});
    numT=length(data);
    data=cell2mat(data');
    numS=length(find(data>Window(1) & data<Window(2)));
    restrate(i-1)=numS/numT/(Window(2)-Window(1));
end
