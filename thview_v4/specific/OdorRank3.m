function OdorRank3(START,END,varargin)

spikesummary=varargin{1}(:,1);
normspikesummary=varargin{1}(:,1);
for i=1:length(varargin)
    Binned=SpikeCountBin(varargin{i},START,END,(END-START)*1000);
    Binned(end,:)=[];
    temp=cell2mat(Binned(:,3));
    temp=temp/max(temp);
    temp=num2cell(temp);
    normspikesummary(:,i+1)=temp;
    spikesummary(:,i+1)=Binned(:,3);
end

spikesummary=sortrows(spikesummary,2);
normspikesummary=sortrows(normspikesummary,2);

figure

subplot(1,2,1)
plot(1:size(spikesummary,1),cell2mat(spikesummary(:,2:end)),...
    'o-')
set(gca,'xlim',[0.5,size(spikesummary,1)+0.5],...
    'xtick',1:size(spikesummary,1),...
    'xticklabel',spikesummary(:,1))

subplot(1,2,2)
plot(1:size(normspikesummary,1),cell2mat(normspikesummary(:,2:end)),...
    'o-')
set(gca,'xlim',[0.5,size(normspikesummary,1)+0.5],...
    'xtick',1:size(normspikesummary,1),...
    'xticklabel',normspikesummary(:,1))