function ConfMat_rawdata(range,bin)
%Perform LDA without using PC.
%'range' is epoch of interest...[START,END] in sec
%'bin' is binsize (ms)



[INPUT,VarName]=selectfromws;
if isempty(INPUT)
    return
end

numdata=length(INPUT);
order=INPUT{1}(:,1);

f = @(xtr,ytr,xte,yte)confusionmat(yte,...
classify(xte,xtr,ytr),'order',order);

figure

for j=1:numdata
    data=INPUT{j};
    data=SpikeCountBin(data,range(1),range(2),bin);
    data(end,:)=[];
    %data(end,:)=[]; %uncomment if you want to remove air responses
    group={};
    for k=1:size(data,1)
        group(end+1:end+size(data{k,2},1))=data(k,1);
    end
    group=group';
    cp=cvpartition(group,'leaveout');
    all=cell2mat(data(:,2));
    
    cfMat=crossval(f,all,group,'partition',cp);
    cfMat=reshape(sum(cfMat),length(order),length(order));
    cfMat=cfMat./(repmat(sum(cfMat,2),1,size(cfMat,2)));
    percentcfMat(:,:,j)=100*cfMat;
    subplot(1,numdata,j)
    imagesc(percentcfMat(:,:,j),[0 100])
    %colormap(gray)
    set(gca,'xtick',1:length(order),...
            'xticklabel',order,...
            'XAxisLocation','top',...
            'ytick',[])
    if j==1
        set(gca,'ytick',1:length(order),...
            'yticklabel',order)
        xlabel('Classified as')
        ylabel('Presented Odor')
    end
    
    
end
CH1=colorbar('SouthOutside');
set(get(CH1,'xlabel'),'string','% Classified')