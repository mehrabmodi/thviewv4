%Combine all selected spike data into one cell named 'SortedSpikeCell'.
%Rename later if needed.
%Rejected sweeps and non-odor sweeps will be ignored.

%The first column is the shortname of presented odor defined in
%'shortodorname.xls'. 
%{n,2} is cell array vector, which contains the spike data evoked by the
%odor {n,1}. The each element of the cell array is the spike time vector
%(sec)

%Order of rows will be determined by 'shortodorname.xls'. 

SortedSpikeCell={'tobedeleted',[]};
[~, odorfieldtable]= xlsread('shortodorname.xls');
roworder=[];

for i=1:length(multifilenum)
        
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        clear Spike_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
        
        if isfield(currentsweep,'odor')
           eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
           eval(['currentspike=Spike_' int2str(multifilenum(i)) ';']) 
           if isempty(currentspike)
               currentspike=single(zeros(0,1));
           end
           odorrow1=strmatch(currentsweep.odor,odorfieldtable(:,1),'exact');
            if isempty(odorrow1)
                disp('Unknown odor...')
                return
            end
            currentodor=odorfieldtable{odorrow1,2};
            odorrow2=strmatch(currentodor,SortedSpikeCell(:,1),'exact');
            if isempty(odorrow2)
                SortedSpikeCell{end+1,1}=currentodor;
                SortedSpikeCell{end,2}={currentspike(:,1)'};
                roworder(end+1)=odorrow1;
            else
                SortedSpikeCell{odorrow2,2}{end+1,1}=currentspike(:,1)';
            end
        end
    end
end
SortedSpikeCell(1,:)=[];
[~,roworder]=sort(roworder);
SortedSpikeCell=SortedSpikeCell(roworder,:);
