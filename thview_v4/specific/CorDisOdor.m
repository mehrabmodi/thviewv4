function CorDisOdor(START,END,BIN)

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end

nn=length(INPUT);

for i=1:nn
    Binned=SpikeCountBin(INPUT{i},START,END,BIN);
    Binned=Binned(1:end-2,3);%remove 'AIR' and 'TIME' row
    Binvec(i,:)=cell2mat(Binned');
    sBinvec(i,:)=cell2mat(Binned(randperm(length(Binned)))');
    
    Srate=SpikeCountBin(INPUT{i},START,END,(END-START)*1000);
    Srate=Srate(1:end-2,3);%remove 'AIR' and 'TIME' row
    Svec(i,:)=cell2mat(Srate');
    sSvec(i,:)=cell2mat(Srate(randperm(length(Srate)))');
end

Bindata=[Binvec;sBinvec];
Sdata=[Svec;sSvec];

figure
set(gcf,'color','w')

subplot(1,2,1)
imagesc(squareform(pdist(Bindata,'correlation')))
CH1=colorbar('SouthOutside');
set(get(CH1,'xlabel'),'string','Correlation Distance')
axis square
title('Bin')

subplot(1,2,2)
imagesc(squareform(pdist(Sdata,'correlation')))
CH2=colorbar('SouthOutside');
set(get(CH2,'xlabel'),'string','Correlation Distance')
axis square
title('Spike Count')
