function plotBeforeAfterDAdata(Concentration,newfig)

F='PSTH200'; %field used for plot
T=[3.5,5];% Time window
minspont=1; %minimum spontaneous fireing rate

if nargin<2
    newfig=1;
end

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end

if newfig
    figure
end

for i=1:length(INPUT)
    data=INPUT{i};
    if data.reject || ~isfield(data,F) || data.DAconc~=Concentration  || data.SpontRate(1)<minspont
        continue
    end
    R=data.(F).mean;
    R(strcmpi('air',R(:,1)),:)=[];
    R(strcmpi('iaa_3',R(:,1)),:)=[];
    R(strcmpi('iaa_4',R(:,1)),:)=[];
    R(strcmpi('iaa_5',R(:,1)),:)=[];
    Time=data.(F).time;
    Tind=find(Time>T(1) & Time<T(2));
    for j=1:size(R,1)
        plot(R{j,2}(Tind,1),R{j,2}(Tind,2),'o')
        hold on
    end
end

axis equal
X=get(gca,'xlim');
Y=get(gca,'yLim');
Lim=[0,max([X(2),Y(2)])];
set(gca,'xlim',Lim,'ylim',Lim)
line([0,Lim(2)],[0,Lim(2)],'color','k');
