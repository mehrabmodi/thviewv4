function rasterfromexpspike(varargin)

%Create raster plot from matrix inputspike
%Each column is spike time with NaN in extra space

rasterX = [];
rasterY = [];
barlength=0.8; %Must be <1
odorcolor=[1 1 .6]; %color of patch
odortime=[3 4]; %odor presentation time
xrange=[2 10]; %x range to plot
linewidth=1; %width of raster

inputspike=[];

for i=1:length(varargin)
    inputspike(1:size(varargin{i},1),end+1:end+size(varargin{i},2))=varargin{i};
end
inputspike(inputspike==0)=nan;

for i=1:size(inputspike,2)
    currentspike=inputspike(:,i);
    currentspike=currentspike(~isnan(currentspike));
    
    n=3*length(currentspike);
    x=NaN(n,1);
    y=x;
    x(1:3:n)=currentspike(:,1);
    x(2:3:n)=currentspike(:,1);
    y(1:3:n)=size(inputspike,2)-i+1+barlength/2;
    y(2:3:n)=size(inputspike,2)-i+1-barlength/2;
        
    rasterX=[rasterX;x];
    rasterY=[rasterY;y];
    
end

%figure('position',[0 495 940 439]);
plot(rasterX,rasterY,'k-','linewidth',linewidth)
set(gca,'Xlim',xrange,...
    'Ylim',[0 i+1],'YTick',1:1:i,...
    'YTickLabel',[],'Tickdir','out','box','off','linewidth',1)
patch([odortime(1);odortime(2);odortime(2);odortime(1)],...
      [.5;.5;i+0.5;i+0.5],[-1;-1;-1;-1],...
      odorcolor,'EdgeColor','none');

