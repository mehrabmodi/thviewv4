function rasterDAcellSingleOdor(DAcell,fig)
%Modified from 'rasterDAcell.m' for single odor data


if nargin<2
    fig=1;
end

if size(DAcell,2)==3
    nowash=1;
else
    nowash=0;
end

Data=cell(2,2);
Data(1,1)={'Before'};
Data(2,1)={'DA'};
if ~nowash
    Data(3,1)={'Wash'};
end
if fig
    figure
end
for i=1:size(DAcell,1)
    Data(1,2)=DAcell(i,2);
    Data(2,2)=DAcell(i,3);
    if ~nowash
        Data(3,2)=DAcell(i,4);
    end
    rasterfromcell2(Data)
    %title(DAcell{i,1})
end
