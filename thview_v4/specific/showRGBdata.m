function showRGBdata(data)

figure('pos',[246,75,800,850],'color','w');
for i=1:6
    if ~isempty(data.vision.red{i,2})
    subplot(6,4,4*(i-1)+1)
    plot(data.vision.time,data.vision.red{i,2},'color',.8*ones(1,3))
    hold on
    plot(data.vision.time,mean(data.vision.red{i,2},1),'r')
    end
    
    if ~isempty(data.vision.green{i,2})
    subplot(6,4,4*(i-1)+2)
    plot(data.vision.time,data.vision.green{i,2},'color',.8*ones(1,3))
    hold on
    plot(data.vision.time,mean(data.vision.green{i,2},1),'g')
    end
    
    if ~isempty(data.vision.blue{i,2})
    subplot(6,4,4*(i-1)+3)
    plot(data.vision.time,data.vision.blue{i,2},'color',.8*ones(1,3))
    hold on
    plot(data.vision.time,mean(data.vision.blue{i,2},1),'b')
    end
end

set(findobj(gcf,'type','axes'),'xlim',[0,3],'ylim',[-60,-25])

for i=1:5
    subplot(6,4,4*i)
    plot(data.odor.time,data.odor.data{i,2},'color',.8*ones(1,3))
    hold on
    plot(data.odor.time,mean(data.odor.data{i,2},1),'k')
    title(data.odor.data{i,1})
    xlim([0,10]);ylim([-60,-25])
end
