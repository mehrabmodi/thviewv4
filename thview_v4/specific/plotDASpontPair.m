function Data=plotDASpontPair(Concentration,newfig)

minspont=0; %minimum spontaneous fireing rate
Xtickpos=3;%position of x tick label (% of y range)
ppos=[1.5,14];%position of ttest text
if nargin<2
    newfig=1;
end
D=[];

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end

for i=1:length(INPUT)
    data=INPUT{i};
    if data.reject || data.DAconc~=Concentration  || data.SpontRate(1)<minspont
        continue
    end
    D=[D;data.SpontRate(1:2)];
end

Data.data=D;
[h,p]=ttest(Data.data(:,1),Data.data(:,2));
Data.p=p;

if p>0.05
    ptext=sprintf('\\itp\\rm=%.2f',p);
elseif p>0.01
    ptext='* \itp\rm<0.05';
elseif p>0.001
    ptext='** \itp\rm<0.01';
else
    ptext='*** \itp\rm<0.001';
end

if newfig
    figure
    set(gcf,'color','w')
end

for i=1:size(Data.data,1)
    plot([1,2],Data.data(i,:),'o-',...
        'color','k',...
        'markersize',20,...
        'linewidth',1.5,...
        'MarkerFaceColor','w')
    hold on
end
box off
ylabel('Spont Firing Rate (Hz)','fontsize',15)
set(gca,'xtick',[1 2],...
    'xticklabel',[],...
    'xlim',[.5,2.5],...
    'ylim',[0,15],...
    'linewidth',2)
Yrange=get(gca,'ylim');
Xpos=Yrange(1)-(Yrange(2)-Yrange(1))*Xtickpos/100;
text(1,Xpos,'Before','fontsize',20,...
    'Horizontal','center',...
    'Vertical','top');
text(2,Xpos,'Dopamine','fontsize',20,...
    'Horizontal','center',...
    'Vertical','top');
text(ppos(1),ppos(2),sprintf('%s (\\itn\\rm=%d)',ptext,size(Data.data,1)),...
    'fontsize',15,...
    'Horizontal','left',...
    'Vertical','top')