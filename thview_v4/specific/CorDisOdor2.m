function CorDisOdor2(START,END,BIN)

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end

nn=length(INPUT);
for j=1:10
    for i=1:nn
        Binned=SpikeCountBin(INPUT{i},START,END,BIN);
        Binned=Binned(1:end-2,3);%remove 'AIR' and 'TIME' row
        Binvec(i,:)=cell2mat(Binned');
        sBinvec(i,:)=cell2mat(Binned(randperm(length(Binned)))');
        
        Srate=SpikeCountBin(INPUT{i},START,END,(END-START)*1000);
        Srate=Srate(1:end-2,3);%remove 'AIR' and 'TIME' row
        Svec(i,:)=cell2mat(Srate');
        sSvec(i,:)=cell2mat(Srate(randperm(length(Srate)))');
    end
    
    Binmat(:,:,j)=squareform(pdist([Binvec;sBinvec],'correlation'));
    Smat(:,:,j)=squareform(pdist([Svec;sSvec],'correlation'));
end
Binmat=mean(Binmat,3);
Smat=mean(Smat,3);

figure
set(gcf,'color','w')

subplot(1,2,1)
imagesc(Binmat,[0,1])
%colormap(gray)
CH1=colorbar('SouthOutside');
set(get(CH1,'xlabel'),'string','Correlation Distance')
axis square
title('Bin')

subplot(1,2,2)
imagesc(Smat,[0,2])
%colormap(gray)
CH2=colorbar('SouthOutside');
set(get(CH2,'xlabel'),'string','Correlation Distance')
axis square
title('Spike Count')