function plotBeforevsSupDA(baseWindow,responseWindow,newfig)
if nargin<3
    newfig=1;
end
[INPUT,VarName]=selectfromws;

for i=1:length(INPUT)
    data=INPUT{i};
    odorspike=odorspikeDAcell(data,baseWindow,responseWindow);
    X(i)=odorspike{end,2};
    before=odorspike{end,2};
    DA=odorspike{end,3};
    Y(i)=100*(before-DA)/before;
end

if newfig
    figure
end
plot(X,Y,'o','markersize',12)
xlabel('Mean odor-evoked spike number (before DA)')
ylabel('% Suppression of odor-evoked spikes by DA (1mM)')
