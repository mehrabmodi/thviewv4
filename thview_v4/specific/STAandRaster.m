function [ax1,ax2]=STAandRaster(STAstr,window,Nspike,ax)
if nargin<4
    figure('color','w');
    ax1=axes('position',[.1,.4,.8,.5]);
    ax2=axes('position',[.1,.1,.8,.3]);
else
    figure(get(ax,'parent'))
    P=get(ax,'position');
    axis(ax,'off')
    ax1=axes('position',[P(1),P(2)+P(4)*3/8,P(3),P(4)*5/8]);
    ax2=axes('position',[P(1),P(2),P(3),P(4)*3/8]);
end
axes(ax1)
STAplot(STAstr,window,Nspike,0);
set(ax1,'Tag','STAtrace')
set(findobj(ax1,'type','line'),'linewidth',1.5,'color','b')
ylabel('mV','fontsize',12)
axes(ax2)
rasterfromSTAstrAlign(STAstr,Nspike,0);
set(ax2,'Tag','STAraster')
xlabel('msec','fontsize',12)

set(ax1,'box','off','xcolor',get(gcf,'color'),...
    'xlim',[-window(1),window(2)],'linewidth',1.5)
set(ax2,'box','off','ycolor',get(gcf,'color'),'linewidth',1.5)
linkaxes([ax1,ax2],'x')
