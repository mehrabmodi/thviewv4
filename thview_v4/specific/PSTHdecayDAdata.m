function PSTHdecayDAdata

%Computes decay time constant from PSTH data in structure 'DAdata', and add
%the fitting result to the original structure

%This code uses double-exponential decay
%Use 'PSTHdecayDAdatasingle.m' for single exponential

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end
fopt=fitoptions('Method','NonlinearLeastSquares',...
    'StartPoint',[20,2,20,1,2,],...
    'Lower',[0,0,0,0,0]);
ffun=fittype('a*exp(-b*x)+c*exp(-d*x)+e','options',fopt);
CO={'b','g','k'};
for i=1:length(INPUT)
    data=INPUT{i};
    fn=fieldnames(data);
    fi=strmatch('PSTH',fn);
    if isempty(fi)
        continue
    end
    for j=1:length(fi)
        F=fn{fi(j)};
        T=data.(F).time';
        R=data.(F).mean;
        R(strcmpi('air',R(:,1)),:)=[];
        R(strcmpi('iaa_3',R(:,1)),:)=[];
        R(strcmpi('iaa_4',R(:,1)),:)=[];
        R(strcmpi('iaa_5',R(:,1)),:)=[];   
        M=[];
        for k=1:size(R,1)
            M(:,:,k)=R{k,2};
        end
        M=mean(M,3);
        Max=[];
        Maxi=[];
        Tf=[];
        Fit={};
        tau=[];
        for k=1:size(M,2)
            [Max(k),Maxi(k)]=max(M(:,k));
            Tf(:,k)=T-T(Maxi(k));
            Fit{k}=fit(Tf(Maxi(k):end,k),M(Maxi(k):end,k),ffun);
            Fit{k}
            cf=coeffvalues(Fit{k});
            tau(k)=(cf(1)*(1/cf(2))+cf(3)*(1/cf(4)))/(cf(1)+cf(3));
        end
        h=figure;
        plot(T,M)
        ylim=get(gca,'YLim');
        for k=1:size(M,2)
            plot(Tf(:,k),M(:,k),CO{k})
            hold on
            plot(Fit{k})
        end
        set(gca,'YLim',ylim)
        zoom xon
        tau
        if isfield(data.(F),'fit')
            disp('Existing tau is...')
            Ptau=data.(F).fit.tau;
            Ptau
        else
            disp('No existing tau')
        end
        disp(sprintf('%s %s',VarName{i},F))
        reply=input('If fit is OK, press enter  ','s');
        if isempty(reply)
            reply='y';
        end
        if ishandle(h)
            close(h)
        end
        switch reply
            case 'y' 
                data.(F).fit.tau=tau;
                data.(F).fit.fitobj=Fit;
                assignin('base',VarName{i},data)
                disp(sprintf('%s %s done',VarName{i},F))
                data.(F).fit.tau
                data.(F).fit.type='double exp';
            otherwise
                disp(sprintf('%s %s NOT done',VarName{i},F))
                continue
        end
    end
end
        
        