%Sort data from RGB visual stim + odor response

clear SortedRGBdata
odorfieldtable={'octanol','Octanol';...
    'mch','MCH';...
    'vinegar','Vinegar';...
    'iaa','Iaa';...
    '2-heptanone','Heptanone'};
IntensityValue=[10,37,65,92,120,200];

SortedRGBdata.vision.red=cell(6,3);
SortedRGBdata.vision.red(:,1)=num2cell(IntensityValue);
SortedRGBdata.vision.green=SortedRGBdata.vision.red;
SortedRGBdata.vision.blue=SortedRGBdata.vision.red;
SortedRGBdata.vision.time=[];

SortedRGBdata.odor.data=cell(size(odorfieldtable,1),3);
SortedRGBdata.odor.data(:,1)=odorfieldtable(:,2);
SortedRGBdata.odor.time=[];

for i=1:length(multifilenum)
    
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        clear Spike_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
        if isempty(strmatch(sprintf('Spike_%d',multifilenum(i)),spikelist,'exact'))
            currentspike=single(zeros(0,1));
        else
            eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
            eval(['currentspike=Spike_' int2str(multifilenum(i)) ';'])
            if isempty(currentspike)
                currentspike=single(zeros(0,1));
            end
        end
        
        if isfield(currentsweep,'odor')
            odorrow=strmatch(currentsweep.odor,odorfieldtable(:,1),'exact');
            SortedRGBdata.odor.data{odorrow,2}=[SortedRGBdata.odor.data{odorrow,2};...
                currentsweep.data.voltage'*1000/currentsweep.amplifier.Vgain];
            SortedRGBdata.odor.data{odorrow,3}=[SortedRGBdata.odor.data{odorrow,3};...
                {currentspike(:,1)'}];
            if isempty(SortedRGBdata.odor.time)
                SortedRGBdata.odor.time=currentsweep.data.time';
            end
        elseif isfield(currentsweep,'Color')
            lightrow=find(IntensityValue==currentsweep.Intensity);
            SortedRGBdata.vision.(currentsweep.Color){lightrow,2}=[SortedRGBdata.vision.(currentsweep.Color){lightrow,2};...
                currentsweep.data.voltage'*1000/currentsweep.amplifier.Vgain];
            SortedRGBdata.vision.(currentsweep.Color){lightrow,3}=[SortedRGBdata.vision.(currentsweep.Color){lightrow,3};...
                {currentspike(:,1)'}];
            if isempty(SortedRGBdata.vision.time)
                SortedRGBdata.vision.time=currentsweep.data.time';
            end
        end
    end
end
