function [correct,Time]=PWLD_princomp(range,bin)

%Perform piece-wise discrimination using PCs.
classf = @(XTRAIN, ytrain,XTEST)(classify(XTEST,XTRAIN,ytrain));

[INPUT,VarName]=selectfromws;
if isempty(INPUT)
    return
end

numdata=length(INPUT);
Time=range(1):bin/1000:range(2)-bin/1000;
Time=Time+bin/2000;

figure

for j=1:numdata
    data=INPUT{j};
    
    for i=1:length(Time)
        Data=SpikeCountBin(data,range(1),Time(i)+bin/2000,bin);
        Data(end,:)=[];
        %data(end,:)=[]; %uncomment if you want to remove air responses
        
        if i==1
            group={};
            mintrial=[];
            for k=1:size(Data,1)
                group(end+1:end+size(Data{k,2},1))=Data(k,1);
                mintrial(k)=size(Data{k,2},1);
            end
            mintrial=min(mintrial);
            group=group';
            randgroup=group(randperm(length(group))');
        end
        cp=cvpartition(group,'leaveout');
        randcp=cvpartition(randgroup,'leaveout');
        all=cell2mat(Data(:,2));
        [~, Pall]=princomp(all);
        tempcorrect=[];
        for m=1:min(size(Pall,2),mintrial-1)
            P=Pall(:,1:m);
            MCR=crossval('mcr',P,group,'predfun',classf,'partition',cp);
            tempcorrect(m)=1-MCR;
        end
        [maxcorrect,optnum]=max(tempcorrect);
        P=Pall(:,1:optnum);
        randMCR=crossval('mcr',P,randgroup,'predfun',classf,'partition',randcp);
        correct(:,i,j)=[maxcorrect;1-randMCR];
        
    end
    subplot(1,numdata,j)
    hold on
    plot(Time,correct(2,:,j),'k:')
    plot(Time,correct(1,:,j),'k-')
    line(get(gca,'xlim'),1/size(Data,1)*[1 1],'color','g','linewidth',1,'linestyle',':')
end


