function DAcelltoDAdata

%Converts DAcell (cell) to DAdata (structure)

[INPUT,VarName]=selectfromws;

for i=1:length(INPUT)
    data.data=INPUT{i};
    data.SpontRate=restrateDAcell(data.data,[0,3]);
    data.Rin=[];
    assignin('base',['DAdata',VarName{i}(7:end)],data)
end