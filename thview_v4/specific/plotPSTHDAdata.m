function [Time,Data]=plotPSTHDAdata

psthfield={'PSTH200','PSTH100'};
odorlist={'AIR'};

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end

for i=1:length(INPUT)
    data=INPUT{i};
    if ~data.reject
        Field=fieldnames(data);
        thisPSTH=strmatch('PSTH',Field);
        if ~isempty(thisPSTH)
            for j=1:length(thisPSTH)
                if ~sum(strcmp(Field{thisPSTH(j)},psthfield))
                    psthfield{end+1}=Field{thisPSTH(j)};
                end
            end
        end
        Odor=data.data(:,1);
        for j=1:length(Odor)
            if ~sum(strcmp(Odor{j},odorlist))
                odorlist{end+1}=Odor{j};
            end
        end
    end
end

odordata0=[num2cell(logical(zeros(size(odorlist')))),odorlist'];
odordata1=[num2cell(logical(ones(size(odorlist')))),odorlist'];
varlist0=[num2cell(logical(zeros(size(VarName')))),VarName'];
varlist1=[num2cell(logical(ones(size(VarName')))),VarName'];

mainh=figure('Visible','off','numbertitle','off','Name','Plot PSTH',...
    'color',[1 .95 .95],'Position',[10,100,910,850]);

seltableh=uitable('parent',mainh,'units','normalized',...
    'position',[.05,.05,.25,.8],'columnname',[],'rowname',[],...
    'data',odordata1,'columnformat',{'logical','char'},...
    'columneditable',logical([1,0]));
selallodorh=uicontrol('parent',mainh,'units','normalized',...
    'position',[.05,.86,.1,.05],'string','Selsect All',...
    'callback',@selallodorhf);
    function selallodorhf(obj,event)
        set(seltableh,'data',odordata1)
    end

unselallodorh=uicontrol('parent',mainh,'units','normalized',...
    'position',[.2,.86,.1,.05],'string','Unselsect All',...
    'callback',@unselallodorhf);
    function unselallodorhf(obj,event)
        set(seltableh,'data',odordata0)
    end

vartableh=uitable('parent',mainh,'units','normalized',...
    'position',[.35,.05,.25,.8],'columnname',[],'rowname',[],...
    'data',varlist1,'columnformat',{'logical','char'},...
    'columneditable',logical([1,0]),...
    'columnwidth',{'auto',150});
selallvarh=uicontrol('parent',mainh,'units','normalized',...
    'position',[.35,.86,.1,.05],'string','Selsect All',...
    'callback',@selallvarhf);
    function selallvarhf(obj,event)
        set(vartableh,'data',varlist1)
    end

unselallvarh=uicontrol('parent',mainh,'units','normalized',...
    'position',[.5,.86,.1,.05],'string','Unselsect All',...
    'callback',@unselallvarhf);
    function unselallvarhf(obj,event)
        set(vartableh,'data',varlist0)
    end

washh=uitable('parent',mainh,'units','normalized',...
    'position',[.65,.85,.29,.025],'columnname',[],'rowname',[],...
    'data',{'  With wash data?','No'},...
    'columnformat',{'char',{'Yes','No'}},...
    'columneditable',logical([0,1]),...
    'columnwidth',{150,'auto'});

fieldh=uitable('parent',mainh,'units','normalized',...
    'position',[.65,.8,.29,.025],'columnname',[],'rowname',[],...
    'data',{'  Which data?','PSTH100'},...
    'columnformat',{'char',psthfield},...
    'columneditable',logical([0,1]),...
    'columnwidth',{150,'auto'});

conch=uitable('parent',mainh,'units','normalized',...
    'position',[.65,.75,.29,.025],'columnname',[],'rowname',[],...
    'data',{'  Concentration?','1 mM'},...
    'columnformat',{'char',{'1 mM','0.1 mM'}},...
    'columneditable',logical([0,1]),...
    'columnwidth',{150,'auto'});

plottypeh=uitable('parent',mainh,'units','normalized',...
    'position',[.65,.7,.29,.025],'columnname',[],'rowname',[],...
    'data',{'  Plot type','Mean'},...
    'columnformat',{'char',{'Mean','Each cell','Each odor'}},...
    'columneditable',logical([0,1]),...
    'columnwidth',{150,'auto'});

ploth=uicontrol('parent',mainh,'units','normalized',...
    'position',[.85,.1,.1,.1],'string','Plot',...
    'callback',@plotpsth);

set(mainh,'visible','on')



    function plotpsth(obj,event)
        
        selectodor=get(seltableh,'data');
        selectodorlist=selectodor(:,2);
        selectodor=selectodorlist(cell2mat(selectodor(:,1)));
        
        varlistdata=get(vartableh,'data');
        selINPUT=INPUT(cell2mat(varlistdata(:,1)));
        
        washselect=get(washh,'data');
        switch washselect{2}
            case 'Yes'
                washselect=1;
            case 'No'
                washselect=0;
        end
        
        fieldselect=get(fieldh,'data');
        fieldselect=fieldselect{2};
        
        conselect=get(conch,'data');
        switch conselect{2}
            case '1 mM'
                conselect=1;
            case '0.1 mM'
                conselect=0.1;
        end
        
        plottype=get(plottypeh,'data');
        switch plottype{2}
            case 'Mean'
                plotmean
            case 'Each cell'
                plotC
            case 'Each odor'
                plotO
        end
        
        
        
        function plotmean
            psthdata=zeros(0,0,0);
            Time=[];
            for k=1:length(selINPUT)
                data=selINPUT{k};
                cellpsthdata=zeros(0,0,0);
                if ~data.reject && isfield(data,fieldselect) && data.DAconc==conselect  && data.SpontRate(1)>1
                    if washselect
                        if size(data.(fieldselect).mean{1,2},2)<3
                            continue
                        end
                    end
                    data=data.(fieldselect);
                    if isempty(Time)
                        Time=data.time;
                    end
                    for l=1:length(selectodor)
                        R=strmatch(selectodor{l},data.mean(:,1),'exact');
                        if isempty(R)
                            continue
                        end
                        subpsthdata=data.mean{R,2};
                        if ~washselect
                            subpsthdata=subpsthdata(:,1:2);
                        end
                        cellpsthdata(:,:,end+1)=subpsthdata;
                    end
                    psthdata(:,:,end+1)=mean(cellpsthdata,3);
                end
            end
            Data=mean(psthdata,3);
            figure
            plot(Time,Data)
            
        end
    end


    function plotC
        
    end

    function plotO
        
    end

end
