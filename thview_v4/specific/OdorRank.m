function OdorRank(SortedSpikeCell,START,END,BIN)

figure
set(gcf,'color','w')

Binned=SpikeCountBin(SortedSpikeCell,START,END,BIN);

%Raster plot
subplot(3,2,[1,3])
rasterfromcell2(SortedSpikeCell)
line([START,START],get(gca,'ylim'),'color','r',...
    'linestyle','--','linewidth',1)
line([END,END],get(gca,'ylim'),'color','r',...
    'linestyle','--','linewidth',1)

%PSTH(non-overlaping window)
subplot(3,2,5)
plot(Binned{end,3},cell2mat(Binned(1:end-1,3)),'linewidth',1)
box off
set(gca,'xlim',[START,END])
xlabel('Time (sec)')
ylabel('Spike Rate (Hz)')
legend(SortedSpikeCell(:,1))
%text(START+.5,max(get(gca,'ylim')),sprintf('%d msec Bin',BIN),'horizontal','left')
title(sprintf('%d msec Bin',BIN),'vertical','top')

%Correlation matrix of PSTH
subplot(3,2,2)
Sizes=[];
for i=1:size(SortedSpikeCell,1)
    Sizes(i)=size(SortedSpikeCell{i,2},1);
end

R=corrcoef(cell2mat(Binned(1:end-1,3))');

labelloc=(Sizes(1)+1)/2;
for i=2:length(Sizes)
    labelloc(i)=(sum(Sizes(1:i-1))+sum(Sizes(1:i))+1)/2;
end

imagesc(R);
CH1=colorbar;
set(get(CH1,'ylabel'),'string','Correlation Coefficient')
axis square
set(gca,'xtick',1:size(SortedSpikeCell,1),...
        'ytick',1:size(SortedSpikeCell,1),...
        'xticklabel',SortedSpikeCell(:,1),...
        'yticklabel',SortedSpikeCell(:,1),...
        'xaxislocation','top')

%Euclidean distance matrix of bin vector
subplot(3,2,4)
ED=squareform(pdist(cell2mat(Binned(1:end-1,3))));
%ED=ED/max(max(ED));
imagesc(ED);
CH2=colorbar;
set(get(CH2,'ylabel'),'string','Euclidean Distance')
axis square
set(gca,'xtick',1:size(SortedSpikeCell,1),...
        'ytick',1:size(SortedSpikeCell,1),...
        'xticklabel',SortedSpikeCell(:,1),...
        'yticklabel',SortedSpikeCell(:,1),...
        'xaxislocation','top')

%dendrogram of bin vector    
subplot(3,2,6)
dendrogram(linkage(pdist(cell2mat(Binned(1:end-1,3)))),...
    'labels',SortedSpikeCell(:,1));
ylabel('Euclidean Distance')
