function rasterDAcell(DAcell,axesdim)

%Create raster plot from 'DAcell'
%'DAcell' is made by combining 3 'SortedSpikeCell'
%1st column is odor names
%2nd is data before dopamine
%3rd is during dopamine
%4th is after washout
%
%This function uses the function 'rasterfromcell2'
%Detail parameter can be set in rasterfromcell2.m code.
%Define tiling pattern of axes
if nargin<2
    axesdim=ceil(sqrt(size(DAcell,1)))*[1,1];
end

if size(DAcell,2)==3
    nowash=1;
else
    nowash=0;
end

Data=cell(2,2);
Data(1,1)={'Before'};
Data(2,1)={'DA'};
if ~nowash
    Data(3,1)={'Wash'};
end

figure
for i=1:size(DAcell,1)
    Data(1,2)=DAcell(i,2);
    Data(2,2)=DAcell(i,3);
    if ~nowash
        Data(3,2)=DAcell(i,4);
    end
    subplot(axesdim(1),axesdim(2),i)
    rasterfromcell2(Data)
    title(DAcell{i,1})
end
