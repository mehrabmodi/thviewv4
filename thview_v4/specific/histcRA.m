function n=histcRA(x,Window,bin,sample)

%Calculates runnning average of histogram count
%
%Input:
%x : vector (original data)
%Window : 2-element vector [min,max] of time window
%bin : bin size used for histogram count
%sample : sampling interval (usually, sample=bin/2)
%
%Output:
%n : vector with length of Window(1):sample:Window(2)

time=Window(1):sample:Window(2);
n=zeros(size(time));
for i=1:length(time)
    n(i)=length(find(x>(time(i)-bin/2) & x<=(time(i)+bin/2)));
end
