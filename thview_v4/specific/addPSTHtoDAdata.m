function addPSTHtoDAdata

%Adds PSTH to DAdata

[INPUT,VarName]=selectfromws;

for i=1:length(INPUT)
    data=INPUT{i};
    data.PSTH200=psthRADAcell(data.data,0,10,200,100,0);
    data.PSTH100=psthRADAcell(data.data,0,10,100,50,0);
    assignin('base',VarName{i},data)
    disp([VarName{i},' done'])
end