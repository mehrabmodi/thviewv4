function plotmeanPSTHDAdata(Concentration,newfig)

F='PSTH200'; %field used for plot
Norm=0;%if 1, data are normalized to the peak of before data
Error=0;%if 1, errorbars are added
Fit=[];%if empty, no fit,
%             if 'single', single exp fit,
%             if 'double', double exp fit

minspont=0; %minimum spontaneous fireing rate

if nargin<2
    newfig=1;
end

psth=zeros(0,0,0);
normpsth=zeros(0,0,0);
T=[];

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end
for i=1:length(INPUT)
    data=INPUT{i};
    odorpsth=zeros(0,0,0);
    if data.reject || ~isfield(data,F) || data.DAconc~=Concentration  || data.SpontRate(1)<minspont
        continue
    end
    if isempty(T)
        T=data.(F).time';
    end
    R=data.(F).mean;
    R(strcmpi('air',R(:,1)),:)=[];
    R(strcmpi('iaa_3',R(:,1)),:)=[];
    R(strcmpi('iaa_4',R(:,1)),:)=[];
    R(strcmpi('iaa_5',R(:,1)),:)=[];
    for j=1:size(R,1)
        odorpsth(:,:,end+1)=R{j,2}(:,1:2);
    end
    if ~isempty(odorpsth)
        cellpsth=mean(odorpsth,3);
        normcellpsth=cellpsth/max(cellpsth(:,1));
        normpsth(:,:,end+1)=normcellpsth;
        psth(:,:,end+1)=cellpsth;
    end
end
meanpsth=mean(psth,3);
normmeanpsth=mean(normpsth,3);

sdpsth=std(psth,0,3);
normsdpsth=std(normpsth,0,3);

sempsth=sdpsth/sqrt(size(psth,3));
normsempsth=normsdpsth/sqrt(size(normpsth,3));

if newfig
    figure
    set(gcf,'color','w')
end

if Norm
    plotdata=normmeanpsth;
    ploterror=normsempsth;
else
    plotdata=meanpsth;
    ploterror=sempsth;
end

if Error
    H1=errorbar(T,plotdata(:,1),ploterror(:,1),'color',[.7 .7 .7]);
    hold on
    H2=errorbar(T,plotdata(:,2),ploterror(:,2),'color',[.7 .7 .7]);
    seterrorbarwidth(H1,.0);
    seterrorbarwidth(H2,.0);
end

plot(T,plotdata,'linewidth',1.5)
hold on

if ~isempty(Fit)
    switch Fit
        case 'double'
            fopt=fitoptions('Method','NonlinearLeastSquares',...
                'StartPoint',[20,2,20,1,2],...
                'Lower',[0,0,0,0,0]);
            ffun=fittype('a*exp(-b*x)+c*exp(-d*x)+e','options',fopt);
            
            [Mb,Ib]=max(plotdata(:,1));
            Xb=T(Ib:end)-T(Ib);
            Yb=plotdata(Ib:end,1);
            Fitb=fit(Xb,Yb,ffun)
            Cb=coeffvalues(Fitb);
            taub=(Cb(1)*(1/Cb(2))+Cb(3)*(1/Cb(4)))/(Cb(1)+Cb(3))
            plotfitXb=T(Ib):.001:T(end);
            for i=1:length(plotfitXb)
                plotfitYb(i)=Cb(1)*exp(-Cb(2)*(plotfitXb(i)-T(Ib)))+Cb(3)*exp(-Cb(4)*(plotfitXb(i)-T(Ib)))+Cb(5);
            end
            plot(plotfitXb,plotfitYb,'--r','linewidth',1.5)
            
            [Md,Id]=max(plotdata(:,2));
            Xd=T(Id:end)-T(Id);;
            Yd=plotdata(Id:end,2);
            Fitd=fit(Xd,Yd,ffun)
            Cd=coeffvalues(Fitd);
            taud=(Cd(1)*(1/Cd(2))+Cd(3)*(1/Cd(4)))/(Cd(1)+Cd(3))
            plotfitXd=T(Id):.001:T(end);
            for i=1:length(plotfitXd)
                plotfitYd(i)=Cd(1)*exp(-Cd(2)*(plotfitXd(i)-T(Id)))+Cd(3)*exp(-Cd(4)*(plotfitXd(i)-T(Id)))+Cd(5);
            end
            plot(plotfitXd,plotfitYd,'--m','linewidth',1.5)
            
        case 'single'
            fopt=fitoptions('Method','NonlinearLeastSquares',...
                'StartPoint',[20,2,2],...
                'Lower',[0,0,0]);
            ffun=fittype('a*exp(-b*x)+c','options',fopt);
            
            [Mb,Ib]=max(plotdata(:,1));
            Xb=T(Ib:end)-T(Ib);
            Yb=plotdata(Ib:end,1);
            Fitb=fit(Xb,Yb,ffun)
            Cb=coeffvalues(Fitb);
            taub=1/Cb(2)
            plotfitXb=T(Ib):.001:T(end);
            for i=1:length(plotfitXb)
                plotfitYb(i)=Cb(1)*exp(-Cb(2)*(plotfitXb(i)-T(Ib)))+Cb(3);
            end
            plot(plotfitXb,plotfitYb,'--r','linewidth',1.5)
            
            [Md,Id]=max(plotdata(:,2));
            Xd=T(Id:end)-T(Id);;
            Yd=plotdata(Id:end,2);
            Fitd=fit(Xd,Yd,ffun)
            Cd=coeffvalues(Fitd);
            taud=1/Cd(2)
            plotfitXd=T(Id):.001:T(end);
            for i=1:length(plotfitXd)
                plotfitYd(i)=Cd(1)*exp(-Cd(2)*(plotfitXd(i)-T(Id)))+Cd(3);
            end
            plot(plotfitXd,plotfitYd,'--m','linewidth',1.5)
    end
end
addOdorPatch([3,4])
box off
set(gca,'linewidth',2,...
    'fontsize',14)
xlabel('Time (sec)','fontsize',16)
ylabel('Firing Rate (Hz)','fontsize',16)
legend('Before','Dopamine',...
    sprintf('\\tau\\fontsize{12}=%.3fsec',taub),...
    sprintf('\\tau\\fontsize{12}=%.3fsec',taud))
legend('boxoff')