function h=addscalebar(Position,Length,varargin)

%Draws scale bar on current axes.
%
%Input
%   'Position'    2-element vector to define starting point of the scale bars
%   'Length'      2-element vector to define the length of X and Y scale
%               bars. Enter 0 if need only one scale bar.  Enter negative
%               value if you need the bar in reverse direction.
%
%Properties (optional)
%   'Xlabel' (string)   Shown near X scale bar
%   'Ylabel' (string)   Shown near Y scale bar
%   'LineWidth' (scalar)    To define width of scale bar (defalt is 2)
%   'Color' (colorspec) To define text and line color
%   'FontSize'  (scalar)    To define font size (defalt is 12)
%
%Output
%   h   handle of scale bar hggroup
%


XL=[];
YL=[];
LW=2;
C='k';
FS=12;

if nargin>2
    for i=1:2:length(varargin)
        parameter=varargin{i};
        value=varargin{i+1};
        switch lower(parameter)
            case 'xlabel'
                XL=value;
            case 'ylabel'
                YL=value;
            case 'linewidth'
                LW=value;
            case 'color'
                C=value;
            case 'fontsize'
                FS=value;
        end
    end
    
end
set(gca,'xlimmode','manual','ylimmode','manual');
h=hggroup;
if abs(Length(1))>0
    line([Position(1),Position(1)+Length(1)],...
        Position(2)*ones(1,2),...
        'LineWidth',LW,'color',C,...
        'parent',h);
end

if abs(Length(2))>0
    line(Position(1)*ones(1,2),...
        [Position(2),Position(2)+Length(2)],...
        'LineWidth',LW,'color',C,...
        'parent',h);
end

if ~isempty(XL)
    if Length(2)>=0
        xalign='top';
    else
        xalign='bottom';
    end
    text(Position(1)+Length(1)/2,Position(2),XL,...
        'fontsize',FS,'color',C,...
        'Horizontal','center','Vertical',xalign,...
        'parent',h);
end

if ~isempty(YL)
    if Length(1)>=0
        yalign='right';
    else
        yalign='left';
    end
    text(Position(1),Position(2)+Length(2)/2,YL,...
        'fontsize',FS,'color',C,...
        'Horizontal',yalign,'Vertical','middle',...
        'parent',h);
end
        