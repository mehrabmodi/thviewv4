function plotRestvsSupDA(Concentration,baseWindow,responseWindow,newfig)

minspont=0; %minimum spontaneous fireing rate
if nargin<4
    newfig=1;
end
[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end
X=[];
Y=[];
for i=1:length(INPUT)
    data=INPUT{i};
    if data.reject || data.DAconc~=Concentration  || data.SpontRate(1)<minspont
        continue
    end
    restrate=restrateDAcell(data.data,baseWindow);
    X(end+1)=restrate(1);
    odorspike=odorspikeDAcell(data.data,baseWindow,responseWindow);
    before=odorspike{end,2};
    DA=odorspike{end,3};
    Y(end+1)=100*(before-DA)/before;
end

if newfig
    figure
    set(gcf,'color','w')
end
plot(X,Y,'ok','markersize',20,...
    'linewidth',1.5)
set(gca,'linewidth',2,...
    'xlim',[-1,14],...
    'ylim',[-250,100],...
    'fontsize',14)
xlabel('Spontaneous spike rate (Hz)')
ylabel('% Suppression of odor-evoked spikes by DA')
box off
%set(gca,'xscale','log')

rX=[ones(length(X),1),X'];
[b,bint,r,rint,stats] = regress(Y',rX);
p=stats(3);
if p>0.05
    ptext=sprintf('\\itp\\rm=%.2f',p);
elseif p>0.01
    ptext='* \itp\rm<0.05';
elseif p>0.001
    ptext='** \itp\rm<0.01';
else
    ptext='*** \itp\rm<0.001';
end
xlim=get(gca,'Xlim');
ylim=get(gca,'Ylim');
line(xlim,[[b(2)*xlim(1),b(2)*xlim(2)]+b(1)],'color','k',...
    'linestyle',':','linewidth',1.5);
text(6,-50,sprintf('R^{2}=%.2f\n%s (\\itn\\rm=%d)',stats(1),ptext,length(X)),...
    'fontsize',14,'horizontal','left','vertical','bottom');
%line([1,1],ylim,'color','r','linestyle',':','linewidth',1.5);
