function STAstr=genSTAstr(loadfile,spikefile,multifilenum,rejectedsweeps,window)

%Makes the structure to do spike-triggered averaging
%call this function after selecting the sweeps you'd like to combine in the
%'thview' window.
%
%Output structure has the following fields:
%   -file: file path for the original data
%   -sweep: row vector of sweep numbers
%   -time: row vector of time (defined by the input 'window')
%   -spike: row vector of cell whose each element contains row vector of
%           presynaptic spike time (sec)
%   -predata: matrix of the presynaptic Vm. Each row is one sweep
%   -postdata: matrix of the postsyanaptic Vm. Each row is one sweep

if nargin<5
    window=[0,2];
end
STAstr.file=loadfile;
sweep=[];
spike=[];
predata=[];
postdata=[];
eval(['load ' loadfile ' Data_' int2str(multifilenum(1))])
eval(['currentsweep=Data_' int2str(multifilenum(1)) ';'])
I=find(currentsweep.data.time>=window(1) & currentsweep.data.time<window(2));
STAstr.time=currentsweep.data.time(I)';

for i=1:length(multifilenum)
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        clear preSpike_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
        eval(['load ' spikefile ' preSpike_' int2str(multifilenum(i))])
        eval(['currentprespike=preSpike_' int2str(multifilenum(i)) ';'])
        if isempty(currentprespike)
            currentprespike=single(zeros(0,1));
        end
        currentpredata=currentsweep.data.voltage2*1000/currentsweep.amplifier2.Vgain;
        currentpostdata=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;
        currentpredata=currentpredata(I)';
        currentpostdata=currentpostdata(I)';
        predata=[predata;currentpredata];
        postdata=[postdata;currentpostdata];
        spike=[spike,{currentprespike(:,1)'}];
        sweep=[sweep,multifilenum(i)];
    end
end
STAstr.sweep=sweep;
STAstr.spike=spike;
STAstr.predata=predata;
STAstr.postdata=postdata;