function rasterfrompairingdata(Data)

barlength=0.8; %Must be <1
odorcolor=[1 1 .6]; %color of patch
odortime=[3 4]; %odor presentation time
xrange=[2 6.5]; %x range to plot
linewidth=1; %width of raster

for i=1:4
    numtraces(i)=length(Data{i,2});
end

figure

for i=1:2
    subdata=[Data{2*i-1,2};Data{2*i,2}];
    subplot(1,2,i)
    rasterX = [];
    rasterY = [];
    for j=1:length(subdata)
        n=3*length(subdata{j});
        x=NaN(n,1);
        y=x;
        x(1:3:n)=subdata{j}';
        x(2:3:n)=subdata{j}';
        y(1:3:n)=length(subdata)-j+1+barlength/2;
        y(2:3:n)=length(subdata)-j+1-barlength/2;
        
        rasterX=[rasterX;x];
        rasterY=[rasterY;y];
    end
    plot(rasterX,rasterY,'k-','linewidth',linewidth)
    set(gca,'Xlim',xrange,...
        'Ylim',[0 j+1],'YTick',[],...
        'YTickLabel',[],'Tickdir','out','box','off','linewidth',1)
    patch([odortime(1);odortime(2);odortime(2);odortime(1)],...
        [.5;.5;j+0.5;j+0.5],[-1;-1;-1;-1],...
        odorcolor,'EdgeColor','none');
    line(xrange,(length(subdata)-numtraces(2*i-1)+0.5)*[1,1],'color','r',...
        'linewidth',1)
    text(xrange(1),numtraces(2*i)+numtraces(2*i-1)/2+0.5,'Before',...
        'fontsize',12,'vertical','bottom','horizontal','center','rotation',90)
    text(xrange(1),numtraces(2*i)/2+0.5,'After',...
        'fontsize',12,'vertical','bottom','horizontal','center','rotation',90)
    title(Data{2*i,1}(1:9))
end