function [time,navg,avg]=STAplot(STAstr,window,Nspike,f,ind,norm)
if nargin<6
    norm=1;
end
if nargin<5
    ind=0;
end
if nargin<4
    f=1;
end
if nargin<3
    Nspike=0;
end
if nargin<2
    window=[100,200];
end
out=[];
data=STAstr.postdata;
N=Nspike;

dT=(STAstr.time(2)-STAstr.time(1))*1000;
bI=round(window(1)/dT);
aI=round(window(2)/dT);

for i=1:size(data,1)
    S=STAstr.spike{i};
    if isempty(S)
        continue
    end
    if Nspike==0
        N=1:length(S);
    end
    for j=1:length(N)
        if N(j)<=length(S)
            [C,cI]=min(abs(STAstr.time-S(N(j))));
            out=[out;data(i,cI-bI:cI+aI)];
        end
    end
end
avg=mean(out,1);
navg=avg-mean(avg(1:bI));
time=-dT*bI:dT:dT*aI;
%time=time(1:end-1);
if f
    figure
end
if ind
    plot(time,out,'color',.8*ones(1,3))
    hold on
end
if norm
    plot(time,navg,'r')
else
    plot(time,avg,'r')
end
hold off