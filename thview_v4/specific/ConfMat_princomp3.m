function [allpercentcfMat,allVec]=ConfMat_princomp3(range,bin)
%Perform LDA using first 'numPC' PC.
%'range' is epoch of interest...[START,END] in sec
%'bin' is binsize (ms)
%numPC=5,bin=150 seems to work fine


[INPUT,VarName]=selectfromws;
if isempty(INPUT)
    return
end

numdata=length(INPUT);
order=INPUT{1}(:,1);

classf = @(XTRAIN, ytrain,XTEST)(classify(XTEST,XTRAIN,ytrain));
f = @(xtr,ytr,xte,yte)confusionmat(yte,...
    classify(xte,xtr,ytr),'order',order);
allVec=[];
figure

for j=1:numdata
    data=INPUT{j};
    data=SpikeCountBin(data,range(1),range(2),bin);
    data(end,:)=[];
    %data(end,:)=[]; %uncomment if you want to remove air responses
    group={};
    mintrial=[];
    for k=1:size(data,1)
        group(end+1:end+size(data{k,2},1))=data(k,1);
        mintrial(k)=size(data{k,2},1);
    end
    mintrial=min(mintrial);
    group=group';
    cp=cvpartition(group,'leaveout');
    all=cell2mat(data(:,2));
    [~, Pall]=princomp(all);
    
    tempcorrect=[];
    for m=1:min(size(Pall,2),mintrial-1)
        P=Pall(:,1:m);
        MCR=crossval('mcr',P,group,'predfun',classf,'partition',cp);
        tempcorrect(m)=1-MCR;
    end
    [maxcorrect,optnum]=max(tempcorrect);
    P=Pall(:,1:optnum);
    
    cfMat=crossval(f,P,group,'partition',cp);
    cfMat=reshape(sum(cfMat),length(order),length(order));
    cfMat=cfMat./(repmat(sum(cfMat,2),1,size(cfMat,2)));
    percentcfMat=100*cfMat;
    subplot(2,numdata,j)
    imagesc(percentcfMat,[0 100])
    %colormap(gray)
    set(gca,'xtick',1:length(order),...
        'xticklabel',order,...
        'XAxisLocation','top',...
        'ytick',[])
    if j==1
        set(gca,'ytick',1:length(order),...
            'yticklabel',order)
        xlabel('Classified as')
        ylabel('Presented Odor')
    end
    allpercentcfMat(:,:,j)=percentcfMat;
    percentcfMat=percentcfMat';
    cfVec=percentcfMat(1:end);
    cfVec(1:length(order)+1:end)=[];
    allVec=[allVec,cfVec];
end
CH1=colorbar('South');
set(get(CH1,'xlabel'),'string','% Classified')

subplot(2,numdata,numdata+1:2*numdata)
gscatter(repmat(1:length(cfVec),1,numdata),...
    allVec,...
    ceil([1:length(cfVec)*numdata]/length(cfVec)))