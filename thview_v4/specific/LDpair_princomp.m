function [correct1,correct2]=LDpair_princomp(range,bin)

classf = @(XTRAIN, ytrain,XTEST)(classify(XTEST,XTRAIN,ytrain));

[INPUT,VarName]=selectfromws;
if isempty(INPUT)
    return
end

numdata=length(INPUT);
correct1=zeros(numdata,2);...Classify CS+ & CS- (change between before & after)
correct2=zeros(numdata,2);...Claasify before & after (difference between CS+ & CS-)

for j=1:numdata
    data=INPUT{j};
    data=SpikeCountBin(data,range(1),range(2),bin);
    data(end,:)=[];
    cspb=ones(size(data{1,2},1),1);
    cspa=2*ones(size(data{2,2},1),1);
    csmb=3*ones(size(data{3,2},1),1);
    csma=4*ones(size(data{4,2},1),1);
    mintrial=min([length(cspb),length(cspa),length(csmb),length(csma)]);
    group1a=[cspb;csmb];
    group1b=[cspa;csma];
    group2a=[cspb;cspa];
    group2b=[csmb;csma];
    cp1a=cvpartition(group1a,'leaveout');
    cp1b=cvpartition(group1b,'leaveout');
    cp2a=cvpartition(group2a,'leaveout');
    cp2b=cvpartition(group2b,'leaveout');
    [~,P1a]=princomp(cell2mat(data([1,3],2)));
    [~,P1b]=princomp(cell2mat(data([2,4],2)));
    [~,P2a]=princomp(cell2mat(data([1,2],2)));
    [~,P2b]=princomp(cell2mat(data([3,4],2)));
    tempcorrect=[];
    for m=1:6 %min(size(P1a,2),mintrial-1)-2
        MCR=[crossval('mcr',P1a(:,1:m),group1a,'predfun',classf,'partition',cp1a),...
            crossval('mcr',P1b(:,1:m),group1b,'predfun',classf,'partition',cp1b),...
            crossval('mcr',P2a(:,1:m),group2a,'predfun',classf,'partition',cp2a),...
            crossval('mcr',P2b(:,1:m),group2b,'predfun',classf,'partition',cp2b)];
        tempcorrect(m,:)=ones(1,4)-MCR;
    end
    maxcorrect=max(tempcorrect);
    correct1(j,:)=maxcorrect(1:2);
    correct2(j,:)=maxcorrect(3:4);
end