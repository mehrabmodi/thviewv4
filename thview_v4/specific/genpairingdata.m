[~, odorfieldtable]= xlsread('shortodorname.xls');
postsweep=input('Enter 1st sweep # after pairing : ');

clear Data_*
eval(['load ' loadfile ' Data_' int2str(multifilenum(1))])
eval(['currentsweep=Data_' int2str(multifilenum(1)) ';'])
CSplus=odorfieldtable{strmatch(currentsweep.odor,odorfieldtable(:,1),'exact'),2};

clear Data_*
eval(['load ' loadfile ' Data_' int2str(multifilenum(2))])
eval(['currentsweep=Data_' int2str(multifilenum(2)) ';'])
CSminus=odorfieldtable{strmatch(currentsweep.odor,odorfieldtable(:,1),'exact'),2};

PairingData=cell(4,2);
PairingData{1,1}=sprintf('%s (CS+) Before',CSplus);
PairingData{2,1}=sprintf('%s (CS+) After',CSplus);
PairingData{3,1}=sprintf('%s (CS-) Before',CSminus);
PairingData{4,1}=sprintf('%s (CS-) After',CSminus);

for i=1:length(multifilenum)
         
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        clear Spike_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
        
        if strcmp(currentsweep.menuname,'Test')
           eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
           eval(['currentspike=Spike_' int2str(multifilenum(i)) ';']) 
           currentodor=odorfieldtable{strmatch(currentsweep.odor,odorfieldtable(:,1),'exact'),2};
           
           switch currentodor
               case CSplus
                   if multifilenum(i)<postsweep
                       rownum=1;
                   else
                       rownum=2;
                   end
               case CSminus
                   if multifilenum(i)<postsweep
                       rownum=3;
                   else
                       rownum=4;
                   end
               otherwise
                   disp('There are more than 3 odors')
                   return
           end
           PairingData{rownum,2}{end+1,1}=currentspike(:,1)';
        end
    end
end