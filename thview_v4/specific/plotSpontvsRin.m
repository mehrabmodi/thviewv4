function plotSpontvsRin(newfig)
if nargin<1
    newfig=1;
end
[INPUT,VarName]=selectfromws;
X=[];Y=[];
for i=1:length(INPUT)
    data=INPUT{i};
    if ~isempty(data.Rin)
        
        X(end+1)=data.SpontRate(1);
        Y(end+1)=100*(data.Rin(1)-data.Rin(2))/data.Rin(1);
    end
end

if newfig
    figure
end
plot(X,Y,'o','markersize',12)
xlabel('Spontaneous spike rate (Hz)')
ylabel('% Suppression of Rin')
set(gca,'xscale','log')