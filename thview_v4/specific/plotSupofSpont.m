function plotSupofSpont(baseWindow,newfig)
if nargin<2
    newfig=1;
end
[INPUT,VarName]=selectfromws;

for i=1:length(INPUT)
    data=INPUT{i};
    restrate=restrateDAcell(data,baseWindow);
    X(i)=restrate(1);
    Y(i)=100*(restrate(1)-restrate(2))/restrate(1);
end

if newfig
    figure
end
plot(X,Y,'o','markersize',12)
xlabel('Spontaneous spike rate (Hz)')
ylabel('% Suppression of spontaneous spikes by DA (1mM)')
set(gca,'xscale','log','ylim',[-80,100])