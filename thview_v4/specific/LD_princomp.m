function [correctall,correctcv]=LD_princomp

%Screen for optimal bin size & number of PCs for LDA.
%Output is correct classification rate with (latter) and without cross
%validation (leave out one method)

range=[3.4 6];
binrange=[40:20:100 150:50:500];

numbintype=length(binrange);
classf = @(XTRAIN, ytrain,XTEST)(classify(XTEST,XTRAIN,ytrain));

[INPUT,VarName]=selectfromws;
if isempty(INPUT)
    return
end

numdata=length(INPUT);

correctall=nan(10,numbintype,numdata);
correctcv=correctall;

for i=1:numbintype
    bin=binrange(i);
    
    for j=1:numdata
        data=INPUT{j};
        data=SpikeCountBin(data,range(1),range(2),bin);
        data(end,:)=[];
        group={};
        for k=1:size(data,1)
            group(end+1:end+size(data{k,2},1))=data(k,1);
        end
        group=group';
        cpall=cvpartition(group,'resubstitution');
        cpcv=cvpartition(group,'leaveout');
        all=cell2mat(data(:,2));
        [~, Pall]=princomp(all);
        for m=1:min(10,size(Pall,2))
            P=Pall(:,1:m);
            MCRall=crossval('mcr',P,group,'predfun',classf,'partition',cpall);
            MCRcv=crossval('mcr',P,group,'predfun',classf,'partition',cpcv);
            correctall(m,i,j)=1-MCRall;
            correctcv(m,i,j)=1-MCRcv;
        end
    end
end