%Modified from "gensortedspikecell.m" for single odor presentaion by new
%odor delivery system.


SortedSpikeCell={'500ms',[]};

for i=1:length(multifilenum)
        
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        clear Spike_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
        
        
           eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
           eval(['currentspike=Spike_' int2str(multifilenum(i)) ';']) 
           if isempty(currentspike)
               currentspike=single(zeros(0,1));
           end
           
            
                SortedSpikeCell{1,2}{end+1,1}=currentspike(:,1)';
                
    end
end


