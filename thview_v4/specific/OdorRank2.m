function OdorRank2(SortedSpikeCell,START,END,BIN)

figure
set(gcf,'color','w')

Binned=SpikeCountBin(SortedSpikeCell,START,END,BIN);
SPrate=SpikeCountBin(SortedSpikeCell,START,END,(END-START)*1000);

%Raster plot
subplot(6,3,1:3:10)
rasterfromcell2(SortedSpikeCell)
line([START,START],get(gca,'ylim'),'color','r',...
    'linestyle','--','linewidth',1)
line([END,END],get(gca,'ylim'),'color','r',...
    'linestyle','--','linewidth',1)

%PSTH(non-overlaping window)
subplot(6,3,[13,16])
plot(Binned{end,3},cell2mat(Binned(1:end-1,3)),'linewidth',1)
box off
set(gca,'xlim',[START,END])
xlabel('Time (sec)')
ylabel('Spike Rate (Hz)')
legend(SortedSpikeCell(:,1))
%text(START+.5,max(get(gca,'ylim')),sprintf('%d msec Bin',BIN),'horizontal','left')
title(sprintf('%d msec Bin',BIN),'vertical','top')

%Euclidean distance matrix of bin vector
subplot(6,3,2:3:8)
ED1=squareform(pdist(cell2mat(Binned(1:end-1,3))));
%ED=ED/max(max(ED));
imagesc(ED1);
CH1=colorbar('SouthOutside');
set(get(CH1,'xlabel'),'string','Euclidean Distance')
axis square
set(gca,'xtick',1:size(SortedSpikeCell,1),...
        'ytick',1:size(SortedSpikeCell,1),...
        'xticklabel',SortedSpikeCell(:,1),...
        'yticklabel',SortedSpikeCell(:,1),...
        'xaxislocation','top')
title('Binned','fontsize',14)

%dendrogram of bin vector 
subplot(6,3,11:3:17)
dendrogram(linkage(pdist(cell2mat(Binned(1:end-1,3)))),...
    'labels',SortedSpikeCell(:,1));
ylabel('Euclidean Distance')

%Euclidean distance matrix of mean spik rate
subplot(6,3,3:3:9)
ED2=squareform(pdist(cell2mat(SPrate(1:end-1,3))));
%ED=ED/max(max(ED));
imagesc(ED2);
CH2=colorbar('SouthOutside');
set(get(CH2,'xlabel'),'string','Spike-Rate Difference')
axis square
set(gca,'xtick',1:size(SortedSpikeCell,1),...
        'ytick',1:size(SortedSpikeCell,1),...
        'xticklabel',SortedSpikeCell(:,1),...
        'yticklabel',SortedSpikeCell(:,1),...
        'xaxislocation','top')
title('Spike Count','fontsize',14)

%dendrogram of mean spike rate 
subplot(6,3,12:3:18)
dendrogram(linkage(pdist(cell2mat(SPrate(1:end-1,3)))),...
    'labels',SortedSpikeCell(:,1));
ylabel('Spike-Rate Difference')