function excludefromlegend(varargin)

for i=1:nargin
    h=varargin{i};
    set(get(get(h,'Annotation'),'LegendInformation'),...
    'IconDisplayStyle','off');
end