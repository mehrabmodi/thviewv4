function rasterfromSTAstrAlign(STAstr,N,f)

barlength=0.8; %Must be <1
linewidth=2; %width of raster

if nargin<3
    f=1;
end

if nargin<2
    N=1;
end

S=STAstr.spike;
rasterX=[];
rasterY=[];

for i=1:length(S)
    n=3*length(S{i});
    x=NaN(n,1);
    y=x;
    x(1:3:n)=S{i}-S{i}(N);
    x(2:3:n)=S{i}-S{i}(N);
    y(1:3:n)=length(S)-i+1+barlength/2;
    y(2:3:n)=length(S)-i+1-barlength/2;
    
    rasterX=[rasterX;x];
    rasterY=[rasterY;y];
end
if f
    figure
end
plot(1000*rasterX,rasterY,'k-','linewidth',linewidth)
set(gca,'Ylim',[0 i+1],'YTick',1:1:i,...
    'YTickLabel',[],'Tickdir','out','box','off','linewidth',1)