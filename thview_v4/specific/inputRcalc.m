base=[0,1];
steady=[1.5,2];

tempaverage=[];

for i=1:length(multifilenum)
    clear Data_*
    eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
    eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
    tempaverage(:,i)=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
end

average=mean(tempaverage,2);%pA
Leak=mean(average(currentsweep.data.time>=base(1) & currentsweep.data.time<base(2)));
average=average-Leak;
Rs=abs(currentsweep.parameter.pulseA/min(average))*1000;%Gohm
Rin=abs(currentsweep.parameter.pulseA/mean(average(currentsweep.data.time>=steady(1) & currentsweep.data.time<steady(2))));%Gohm

disp(sprintf('Rs = %.3gMOhm',Rs))
disp(sprintf('Rin = %.3gGOhm',Rin))
disp(sprintf('Leak = %.3gpA',Leak))