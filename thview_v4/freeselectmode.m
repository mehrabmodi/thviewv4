freeselection=[];
if get(freeselecth,'value')==1
    set(lbxh,'value',filenum)
    set(lbxh,'callback','memorylbxfcn','max',1)
    zoom off
    pan off
    set(anawinh,'Pointer','custom')
else
    set(lbxh,'callback','lbxfcn','max',2)
    set(anawinh,'Pointer','arrow')
end
