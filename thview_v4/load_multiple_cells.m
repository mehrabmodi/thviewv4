[FileName_list,PathName_list]=uigetfile('D:\Data\Janelia\Patch\*xls','Select Cell List File');

%reading in cell list file
[~,~,cell_list] = xlsread([PathName_list FileName_list]);
n_cells = size(cell_list, 1);

cell_n = 1;

FileName = cell_list(cell_n, 2);
FileName = FileName{1, 1};
PathName = cell_list(cell_n, 1);
PathName = PathName{1, 1};
PathName = [PathName, '\'];

loadfcn_list;
