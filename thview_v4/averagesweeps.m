averagedsweepy=[];
averagedsweepx=[];
tempaverage=[];

for i=1:length(multifilenum)
    if ~rejectedsweeps(multifilenum(i))
    clear Data_*
    eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
    eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
    switch currentsweep.amplifier.Mode
        case 'I-Clamp'
            tempaverage=[tempaverage,currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain];
        case 'V-Clamp'
            tempaverage(:,i)=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
    end
    end
end

averagedsweepy=mean(tempaverage,2);
averagedsweepx=double(currentsweep.data.time);

clear tempaverage