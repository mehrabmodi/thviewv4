function PlotMultiRaster

[INPUT,VarName]=selectfromws;

if isempty(INPUT)
    return
end

figure

nn=length(INPUT);

for i=1:nn
    subplot(1,nn,i)
    rasterfromcell2(INPUT{i})
    title(VarName{i})
end