set(manualh,'enable','off')
spikearea=getrect(ploth);
spikeareaX=[spikearea(1);spikearea(1)+spikearea(3);...
            spikearea(1)+spikearea(3);spikearea(1);spikearea(1)];
spikeareaY=[spikearea(2);spikearea(2);...
            spikearea(2)+spikearea(4);spikearea(2)+spikearea(4);spikearea(2)];
plotlimX=get(ploth,'Xlim');
plotlimY=get(ploth,'Ylim');

if ~isempty(currentprespike)
    IN=inpolygon(currentprespike(:,1),currentprespike(:,2),spikeareaX,spikeareaY);
    % If any spike is selected with rectancle, the spikes will be deleted.
    if max(IN)>0
        currentprespike(IN,:)=[];
        eval(['preSpike_' int2str(filenum) '=currentprespike;'])
        eval(['save ' spikefile ' preSpike_' int2str(filenum) ' -append'])
        lbxfcn
        axis([plotlimX plotlimY])
        set(manualh,'enable','on')
        return
    end
end

IN=inpolygon(currentsweep.data.time,currentsweep.data.voltage2*1000/currentsweep.amplifier2.Vgain,spikeareaX,spikeareaY);
% If no spike is selected with rectancle, the max voltage point will be added as a new spike.
if max(IN)>0
    selectedDATA=[currentsweep.data.time(IN),currentsweep.data.voltage2(IN)*1000/currentsweep.amplifier2.Vgain];
    newspike=[selectedDATA(find(selectedDATA(:,2)==max(selectedDATA(:,2)),1,'first'),1),...
              selectedDATA(find(selectedDATA(:,2)==max(selectedDATA(:,2)),1,'first'),2)];
    currentprespike=[currentprespike;newspike];
    currentprespike=sortrows(currentprespike,1);
    % Save in the spike file
    eval(['preSpike_' int2str(filenum) '=currentprespike;'])
    if exist(spikefile,'file')
        eval(['save ' spikefile ' preSpike_' int2str(filenum) ' -append'])
    else
        eval(['save ' spikefile ' preSpike_' int2str(filenum)])
    end
    
    
    % Update the spike list
    if isempty(strmatch(sprintf('preSpike_%d',filenum),prespikelist,'exact'))
        prespikelist=[prespikelist;sprintf('preSpike_%d',filenum)];
    end
    
    % Update the plot
    lbxfcn
    axis([plotlimX plotlimY])
    
end
set(manualh,'enable','on')