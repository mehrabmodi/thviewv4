function odorspike=odorspikeDAcell(DAcell,baseWindow,responseWindow)
%Calculates number of odor-evoked spikes per trial
%spike count within 'responseWindow' is subtracted by spontaneous spikes
%calculated in 'baseWindow'

DAcell(strcmpi('air',DAcell(:,1)),:)=[];
DAcell(strcmpi('iaa_3',DAcell(:,1)),:)=[];
DAcell(strcmpi('iaa_4',DAcell(:,1)),:)=[];
DAcell(strcmpi('iaa_5',DAcell(:,1)),:)=[];
odorspike=cell(size(DAcell));
odorspike(:,1)=DAcell(:,1);

for i=2:size(DAcell,2)
    for j=1:size(DAcell,1)
        data=DAcell{j,i};
        numT=length(data);
        data=cell2mat(data');
        numS=length(find(data>baseWindow(1) & data<baseWindow(2)));
        numR=length(find(data>responseWindow(1) & data<responseWindow(2)));
        odorspike{j,i}=numR/numT-numS/numT/(baseWindow(2)-baseWindow(1))*(responseWindow(2)-responseWindow(1));
    end
end

odorspike(end+1,1)={'mean'};
odorspike(end,2:end)=num2cell(mean(cell2mat(odorspike(1:end-1,2:end))),1);