function S=FiltAvgSave(f)
loadfile=evalin('base','loadfile');
multifilenum=evalin('base','multifilenum');
rejectedsweeps=evalin('base','rejectedsweeps');
[~, odorfieldtable]= xlsread('shortodorname.xls');
eval(['load ' loadfile ' Data_' int2str(multifilenum(1))])
eval(['sf=Data_' int2str(multifilenum(1)) '.parameter.ai_sr;'])
span=floor(round(sf/f)/2)*2+1;
roworder=[];

C={'tobedeleted',[],[]};

for i=1:length(multifilenum)
    
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['D=Data_' int2str(multifilenum(i)) ';'])
        if isfield(D,'odor')
            r1=strmatch(D.odor,odorfieldtable(:,1),'exact');
            if isempty(r1)
                disp('Unknown odor...')
                return
            end
            O=odorfieldtable{r1,2};
            r2=strmatch(O,C(:,1),'exact');
            if isempty(r2)
                C{end+1,1}=O;
                C{end,2}=smooth(D.data.voltage*1000/D.amplifier.Vgain,span)';
                roworder(end+1)=r1;
                C{end,3}=1;
            else
                C{r2,2}(end+1,:)=smooth(D.data.voltage*1000/D.amplifier.Vgain,span)';
                C{r2,3}=C{r2,3}+1;
            end
        end
    end
end
C(1,:)=[];
nonair=[];
nonairL={};
for i=1:size(C,1)
    C{i,2}=mean(C{i,2},1);
    if ~isempty(strmatch(C{i,1},odorfieldtable(1:9,2),'exact'))
        nonair(end+1,:)=C{i,2};
        nonairL{end+1}=C{i,1};
    end
end
[~,roworder]=sort(roworder);
C=C(roworder,:);

S.data=C;
S.filter=f;
S.time=D.data.time;
S.odormean=mean(nonair,1);
figure;
plot(S.time,nonair)
legend(nonairL)
hold on
plot(S.time,S.odormean,'k','linewidth',2)
uicontrol('style','push','unit','normalized','pos',[.7 .01 .1 .05],...
    'string','Add','callback',{@ADDdata,loadfile,S});
uicontrol('style','push','unit','normalized','pos',[.85 .01 .1 .05],...
    'string','Cancel','callback',@CANdata);
end

function ADDdata(obj,e,loadfile,S)
eval(['filt' loadfile(end-11:end-6) '_' loadfile(end-4) '=S;'])
eval(['save D:\Data_TH\Summary\FilteredResponse.mat filt' loadfile(end-11:end-6) '_' loadfile(end-4) ' -append'])
close(get(obj,'parent'))
end

function CANdata(obj,e)
close(get(obj,'parent'))
end


