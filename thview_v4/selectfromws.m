function [INPUT,VarName]=selectfromws

%Allow users to select variables from workspace from listbox, pass them to
%the next function as 'INPUT'. The original variables names are output as
%'VarName'.

currentvar=evalin('base','who');
[SelectedVars,OK]=listdlg('ListString',currentvar,...
                           'PromptString','Select variables',...
                           'Name','Variable selection',...
                           'InitialValue',[]);
if isempty(SelectedVars)
   INPUT=[];
   VarName=[];
   return
end

for i=1:length(SelectedVars)
    INPUT{i}=evalin('base',currentvar{SelectedVars(i)});
    VarName{i}=currentvar{SelectedVars(i)};
end
