
if length(get(lbxh,'Value'))==1
    set(anawinh,'CurrentAxes',ploth)
    filenum=get(lbxh,'Value');
    multifilenum=filenum;
    
    if rejectedsweeps(filenum)
        set(rejectstatush,'visible','on')
        set(rejecth,'string','Accept')
    else
        set(rejectstatush,'visible','off')
        set(rejecth,'string','Reject')
    end
    
    clear Data_*
    clear Spike_*
    clear preSpike_*
    eval(['load ' loadfile ' Data_' int2str(filenum) ';'])
    eval(['currentsweep=Data_' int2str(filenum) ';'])
    if ~isempty(strmatch(sprintf('Spike_%d',filenum),spikelist,'exact'))
        eval(['load ' spikefile ' Spike_' int2str(filenum) ';'])
    try    
        eval(['currentspike=Spike_' int2str(filenum) ';'])
    catch
        keyboard
    end
    
    else
        currentspike=[];
    end
    
    if ~isempty(strmatch(sprintf('preSpike_%d',filenum),prespikelist,'exact'))
        eval(['load ' spikefile ' preSpike_' int2str(filenum) ';'])
        eval(['currentprespike=preSpike_' int2str(filenum) ';'])
    else
        currentprespike=[];
    end
    set(tableRh,'data',currentsweep.paratable(:,3:4))
    set(tableRh,'rowname',currentsweep.paratable(:,2))
    set(tableRh,'visible','on')
    
    plotwhatlist=fieldnames(currentsweep.data)';
    plotwhatlist(strcmpi('time',plotwhatlist))=[];
    %     plotwhatlist(strcmpi('voltage',plotwhatlist))=[];
    %     plotwhatlist(strcmpi('current',plotwhatlist))=[];
    if isfield(currentsweep,'ACparameter')
        plotwhatlist=[plotwhatlist,'Odor command'];
    end
    plotwhatlist=['V or I',plotwhatlist,'None'];
    if ~sum(strcmpi(plotwhatdata{1},plotwhatlist))
        plotwhatdata{1}='V or I';
    end
    if ~sum(strcmpi(plotwhatdata{2},plotwhatlist))
        plotwhatdata{2}='None';
    end
    set(plotwhath,'data',plotwhatdata,'ColumnFormat',{plotwhatlist})
    
    switch plotwhatdata{1}
        case 'None'
            return
        case 'V or I'
            switch currentsweep.amplifier.Mode
                case 'I-Clamp'
                    Y1=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;
                    set(Ylabelh,'string','mV')
                case 'V-Clamp'
                    if get(zerosubh,'value')
                        currentsweep.data.current=currentsweep.data.current-mean(currentsweep.data.current);
                    end
                    Y1=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
                    set(Ylabelh,'string','pA')
            end
        case 'voltage'
            Y1=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;
            set(Ylabelh,'string','mV')
        case 'current'
            if get(zerosubh,'value')
                currentsweep.data.current=currentsweep.data.current-mean(currentsweep.data.current);
            end
            Y1=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
            set(Ylabelh,'string','pA')
        case 'voltage2'
            Y1=currentsweep.data.voltage2*1000/currentsweep.amplifier2.Vgain;
            set(Ylabelh,'string','mV')
        case 'current2'
            if get(zerosubh,'value')
                currentsweep.data.current2=currentsweep.data.current2-mean(currentsweep.data.current2);
            end
            Y1=currentsweep.data.current2*1000/currentsweep.amplifier2.Igain;
            set(Ylabelh,'string','pA')
        case 'Odor command'
            Y1=currentsweep.ACparameter.command;
            set(Ylabelh,'string','')
        otherwise
            
            Y1=currentsweep.data.(plotwhatdata{1});
            set(Ylabelh,'string','V')
    end
    
    switch plotwhatdata{2}
        case 'None'
            Y2=0;
        case 'V or I'
            switch currentsweep.amplifier.Mode
                case 'V-Clamp'
                    Y2=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;
                case 'I-Clamp'
                    if get(zerosubh,'value')
                        currentsweep.data.current=currentsweep.data.current-mean(currentsweep.data.current);
                    end
                    Y2=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
            end
        case 'voltatge'
            Y2=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;
        case 'current'
            if get(zerosubh,'value')
                currentsweep.data.current=currentsweep.data.current-mean(currentsweep.data.current);
            end
            Y2=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
        case 'voltage2'
            Y2=currentsweep.data.voltage2*1000/currentsweep.amplifier2.Vgain;
        case 'current2'
            if get(zerosubh,'value')
                currentsweep.data.current2=currentsweep.data.current2-mean(currentsweep.data.current2);
            end
            Y2=currentsweep.data.current2*1000/currentsweep.amplifier2.Igain;
        case 'Odor command'
            if isfield(currentsweep,'ACparameter')
                Y2=currentsweep.ACparameter.command;
            else
                Y2=0;
            end
        otherwise
            Y2=currentsweep.data.(plotwhatdata{2});
    end
    
    
    
    if Y2==0
        plot(ploth,currentsweep.data.time,Y1)
        
    else
        AX=plotyy(currentsweep.data.time,Y1,currentsweep.data.time,Y2);
        
    end
    switch plotwhatdata{1}
        case 'voltage2'
            set(allh,'callback','preAllTraceSpike')
            set(singleh,'callback','preSingleTraceSpike')
            set(multipleh,'Callback','preMultiTraceSpike')
            set(manualh,'callback','premanualspike')
            if ~isempty(currentprespike)&& get(showspikeh,'value')
                hold on
                plot(ploth,currentprespike(:,1),currentprespike(:,2),'rO')
            end
        case {'V or I','voltage'}
            set(allh,'callback','AllTraceSpike')
            set(singleh,'callback','SingleTraceSpike')
            set(multipleh,'Callback','MultiTraceSpike')
            set(manualh,'callback','manualspike')
            if ~isempty(currentspike)&& get(showspikeh,'value')
                hold on
                plot(ploth,currentspike(:,1),currentspike(:,2),'rO')
            end
    end
    
    switch plotwhatdata{2}
        case 'voltage2'
            if ~isempty(currentprespike)&& get(showspikeh,'value')
                hold(AX(2),'on')
                plot(AX(2),currentprespike(:,1),currentprespike(:,2),'rO')
                hold(AX(2),'off')
            end
        case 'voltage'
            if ~isempty(currentspike)&& get(showspikeh,'value')
                hold(AX(2),'on')
                plot(AX(2),currentspike(:,1),currentspike(:,2),'rO')
                hold(AX(2),'off')
            end
    end
    grid on
    if get(fixXscaleh,'value')
        set(findobj(anawinh,'type','axes'),'xlim',XLIM)
    end
    
    if get(fixYscaleh,'value')
        set(ploth,'ylim',YLIM)
    end
    hold off
    set(acqtimeh,'String',currentsweep.time)
    
else
    multifilenum=get(lbxh,'Value');
end