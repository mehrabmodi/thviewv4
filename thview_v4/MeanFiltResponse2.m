function MeanFiltResponse2(Zsub)
if nargin<1
    Zsub=[];
end
load D:\Data_TH\Summary\FilteredResponse.mat
L=who;
L(1)=[];
[SelectedVars,OK]=listdlg('ListString',L,...
    'PromptString','Select data',...
                           'Name','Data selection',...
                           'InitialValue',[]);
if isempty(SelectedVars)
   return
end
M=[];
for i=1:length(SelectedVars)
    eval(['S=' L{SelectedVars(i)} ';'])
    temp=[];
    for j=1:size(S.data,1)
%        if ~isempty(strmatch(S.data{j,1},{'IAA','HEP','MCH','OCT','BZA','HEX'},'exact'))
        if ~isempty(strmatch(S.data{j,1},{'IAA'},'exact'))    
            temp(end+1,:)=S.data{j,2};
        end
    end
    if ~isempty(temp)
        if isempty(Zsub)
            M(end+1,:)=mean(temp,1);
        else
            temp=mean(temp,1);
            M(end+1,:)=temp-mean(temp(S.time>=Zsub(1) & S.time<Zsub(2)));
        end
    end
end
eval(['T=' L{1} '.time;'])
figure;
plot(T,M,'color',.5*ones(1,3))
hold on
plot(T,mean(M,1),'r','linewidth',2)