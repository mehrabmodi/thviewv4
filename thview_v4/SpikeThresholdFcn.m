function Out=SpikeThresholdFcn(Threshold,Onset)
loadfile=evalin('base','loadfile');
spikefile=evalin('base','spikefile');
multifilenum=evalin('base','multifilenum');
rejectedsweeps=evalin('base','rejectedsweeps');
[~, odorfieldtable]= xlsread('shortodorname.xls');
minSep=.01;
Spont=[];
Stim=[];
Base=[];
for i=1:length(multifilenum)
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        clear Spike_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['D=Data_' int2str(multifilenum(i)) ';'])
        eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
        eval(['S=Spike_' int2str(multifilenum(i)) ';'])
        if isfield(D,'odor') && ~isempty(S)
            S=S(:,1);
            SpontS=S(S<=Onset);
            T=D.data.time;
            V=D.data.voltage*1000/D.amplifier.Vgain;
            Base(end+1)=mean(V(T<Onset));
            if ~isempty(SpontS)
                if SpontS(1)<minSep
                    SpontS(1)=[];
                end
                for j=1:length(SpontS)
                    if j==1 || SpontS(j)-SpontS(j-1)>.05
                        vv=V(T>(SpontS(j)-minSep) & T<=SpontS(j));
                        dvv=diff(vv)/1000*D.parameter.ai_sr;
                        pp=1:length(dvv)-1;
                        Spont(end+1)=vv(find(dvv(pp)<Threshold & dvv(pp+1)>=Threshold,1,'last')+1);
                    end
                end
            end
            if ~isempty(strmatch(D.odor,odorfieldtable(2:9,1),'exact'))...
                    && ~isempty(find(S>Onset+minSep,1,'first'))
                StimS=S(find(S>Onset+minSep,1,'first'));
                vv=V(T>(StimS-minSep) & T<=StimS);
                dvv=diff(vv)/1000*D.parameter.ai_sr;
                pp=1:length(dvv)-1;
                Stim(end+1)=vv(find(dvv(pp)<Threshold & dvv(pp+1)>=Threshold,1,'last')+1);
            end
        end
    end
end
figure;
boxplot([Spont,Stim],[ones(size(Spont)),2*ones(size(Stim))])
line(xlim,mean(Base)*[1,1],'color','r')
Y=ylim;
set(gca,'ylim',[-65,Y(2)])
Out.Spont=Spont;
Out.Stim=Stim;
Out.Rest=mean(Base);
Out.Slope=Threshold;
uicontrol('style','push','unit','normalized','pos',[.7 .01 .1 .05],...
    'string','Add','callback',{@ADDdata,loadfile,Out});
uicontrol('style','push','unit','normalized','pos',[.85 .01 .1 .05],...
    'string','Cancel','callback',@CANdata);
end

function ADDdata(obj,e,loadfile,Out)
eval(['thresh' loadfile(end-11:end-6) '_' loadfile(end-4) '=Out;'])
eval(['save D:\Data_TH\Summary\SpikeThreshold.mat thresh' loadfile(end-11:end-6) '_' loadfile(end-4) ' -append'])
close(get(obj,'parent'))
end

function CANdata(obj,e)
close(get(obj,'parent'))
end

