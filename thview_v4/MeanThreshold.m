function [Avg,Med]=MeanThreshold
load D:\Data_TH\Summary\SpikeThreshold.mat
L=who;
[SelectedVars,OK]=listdlg('ListString',L,...
    'PromptString','Select data',...
                           'Name','Data selection',...
                           'InitialValue',[]);
if isempty(SelectedVars)
   return
end
Avg=[];
Med=[];
for i=1:length(SelectedVars)
    eval(['S=' L{SelectedVars(i)} ';'])
    Avg(end+1,:)=[mean(S.Spont),mean(S.Stim)]-S.Rest;
    Med(end+1,:)=[median(S.Spont),median(S.Stim)]-S.Rest;
end
