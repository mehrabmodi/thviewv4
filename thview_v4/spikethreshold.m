spikeshoulder=1; % (V/sec) set the threshold of spike initiation point
%Each trace must contain only 1 spike!!
Threshold=[];%[V threshold;I threshold]
figure

for i=1:length(multifilenum)
    clear Data_*
    clear Spike_*
    eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
    eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
    eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
    eval(['currentspike=Spike_' int2str(multifilenum(i)) ';'])
    
    if size(currentspike,1)~=1
        errordlg('Only 1 spike is allowed!','Spike number error','modal')
        return
    end
    
    V=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;% V is raw voltage data (mV)
    T=currentsweep.data.time;% T is time vector (sec)
    dV=diff(V)/1000*currentsweep.parameter.ai_sr;%dV is 1st derivative of V (V/sec)

    pp=1:round(currentspike(1)*currentsweep.parameter.ai_sr);
    shoulder=find(dV(pp)<spikeshoulder & dV(pp+1)>=spikeshoulder,1,'last');
    
    Threshold(1,i)=V(shoulder);%This is voltage threshold
    Threshold(2,i)=(T(shoulder)-currentsweep.parameter.preP)*...
                               currentsweep.parameter.pulseA/...
                               currentsweep.parameter.pulseD*1000;%This is current threshold
    
    plot(T,V)
    hold on
    plot(T(shoulder),V(shoulder),'r+')
    
end

Threshold=mean(Threshold,2);
Threshold(3,1)=spikeshoulder;