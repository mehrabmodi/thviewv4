%load data from multiple files,
%sort them with presented odors,
%and make a structure named 'OdorData' 

gonext=1;
OdorData=struct;
[~, odorfieldtable]= xlsread('shortodorname.xls');

while gonext
    [FileName,PathName]=uigetfile('D:\Data_TH\*mat','Select Data File');
    loadfile=[PathName FileName];
    numsweep=size(who('Data_*','-file',loadfile),1);
     for i=1:numsweep
        eval(['load ' loadfile ' Data_' int2str(i) ';'])
        eval(['currentsweep=Data_' int2str(i) ';'])

        if isfield(currentsweep,'odor')
            odorrow=strmatch(currentsweep.odor,odorfieldtable(:,1),'exact');
            if isempty(odorrow)
                disp('Unknown odor...')
                return
            end
            currentodorfield=odorfieldtable{odorrow,2};
            if isfield(OdorData,currentodorfield)
                eval(['currentmat=OdorData.' currentodorfield ';'])
                currentmat=[currentmat, currentsweep.data.voltage];
                eval(['OdorData.' currentodorfield '=currentmat;'])
            else
                eval(['OdorData.' currentodorfield '=currentsweep.data.voltage;'])
            end
        end
        clear Data_*
     end
     Continue=questdlg('Combine another file?','Continue?','Yes','No','No');
     switch Continue
         case 'Yes'
         otherwise
             gonext=0;
     end
     OdorData.time=currentsweep.data.time;
end
            