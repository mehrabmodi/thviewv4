function R=PSTHcc(SortedSpikeCell,START,END,BIN)

Binned=SpikeCountBin(SortedSpikeCell,START,END,BIN);
meanPSTH=[];
Sizes=[];
for i=1:size(Binned,1)
    meanPSTH(i,:)=mean(Binned{i,2});
    Sizes(i)=size(Binned{i,2},1);
end

R=corrcoef(meanPSTH');
Rall=corrcoef(cell2mat(Binned(:,2))');

labelloc=(Sizes(1)+1)/2;
for i=2:length(Sizes)
    labelloc(i)=(sum(Sizes(1:i-1))+sum(Sizes(1:i))+1)/2;
end

figure
subplot(1,2,1)
imagesc(R);
colorbar
axis square
set(gca,'xtick',1:size(Binned,1),...
        'ytick',1:size(Binned,1),...
        'xticklabel',Binned(:,1),...
        'yticklabel',Binned(:,1),...
        'xaxislocation','top')

subplot(1,2,2)
imagesc(Rall);
colorbar
axis square
set(gca,'xtick',labelloc,...
        'ytick',labelloc,...
        'xticklabel',Binned(:,1),...
        'yticklabel',Binned(:,1),...
        'xaxislocation','top')