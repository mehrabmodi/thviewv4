exportedspike=[];

if get(freeselecth,'value')==1
    multifilenum=freeselection;
end

for i=1:length(multifilenum)
    clear Spike_*
    
    if isempty(strmatch(sprintf('Spike_%d',multifilenum(i)),spikelist,'exact'))
        errordlg('Detect spikes before creating raster plot!','Spike Not Found','modal')
        return
    end
    
    eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
    eval(['currentspike=Spike_' int2str(multifilenum(i)) ';'])
    
    if isempty(currentspike)
        currentspike=[0 0];%%To avoid error when calling currentspile(:,1)
    end
    
    exportedspike(1:size(currentspike,1),end+1)=currentspike(:,1);
end

exportedspike=[multifilenum;exportedspike];
exportedspike(exportedspike==0)=nan;

set(freeselecth,'value',0)
freeselectmode