clear averagedsweep
averagedsweep.odor='tobedeleted';

for i=1:length(multifilenum)
    if rejectedsweeps(multifilenum(i))==0
        clear Data_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])

        if isempty(strmatch(currentsweep.odor,{averagedsweep.odor},'exact'))
            averagedsweep(end+1).odor=currentsweep.odor;
            averagedsweep(end).sweep=multifilenum(i);
            averagedsweep(end).time=currentsweep.data.time;
            averagedsweep(end).all=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
        else
            averagedsweep(strmatch(currentsweep.odor,{averagedsweep.odor},'exact'))...
                .sweep(end+1)=multifilenum(i);
            averagedsweep(strmatch(currentsweep.odor,{averagedsweep.odor},'exact'))...
                .all(:,end+1)=currentsweep.data.current*1000/currentsweep.amplifier.Igain;
        end
    end
end

averagedsweep=averagedsweep(2:end);
[sortedodor,sortidx]=sort({averagedsweep.odor});
averagedsweep=averagedsweep(sortidx);

for i=1:length(averagedsweep)
    averagedsweep(i).average=mean(averagedsweep(i).all,2);
end