set(manualh,'enable','off')
spikearea=getrect(ploth);
spikeareaX=[spikearea(1);spikearea(1)+spikearea(3);...
    spikearea(1)+spikearea(3);spikearea(1);spikearea(1)];
spikeareaY=[spikearea(2);spikearea(2);...
    spikearea(2)+spikearea(4);spikearea(2)+spikearea(4);spikearea(2)];
plotlimX=get(ploth,'Xlim');
plotlimY=get(ploth,'Ylim');

if ~isempty(currentspike)
    IN=inpolygon(currentspike(:,1),currentspike(:,2),spikeareaX,spikeareaY);
    % If any spike is selected with rectancle, the spikes will be deleted.
    if max(IN)>0
        currentspike(IN,:)=[];
        eval(['Spike_' int2str(filenum) '=currentspike;'])
        eval(['save ' spikefile ' Spike_' int2str(filenum) ' -append'])
        lbxfcn
        axis([plotlimX plotlimY])
        set(manualh,'enable','on')
        return
    end
end
switch currentsweep.amplifier.Mode
    case 'I-Clamp'
        IN=inpolygon(currentsweep.data.time,currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain,spikeareaX,spikeareaY);
        % If no spike is selected with rectancle, the max voltage point will be added as a new spike.
        if max(IN)>0
            selectedDATA=[currentsweep.data.time(IN),currentsweep.data.voltage(IN)*1000/currentsweep.amplifier.Vgain];
            newspike=[selectedDATA(find(selectedDATA(:,2)==max(selectedDATA(:,2)),1,'first'),1),...
                selectedDATA(find(selectedDATA(:,2)==max(selectedDATA(:,2)),1,'first'),2)];
            currentspike=[currentspike;newspike];
            currentspike=sortrows(currentspike,1);
            % Save in the spike file
            eval(['Spike_' int2str(filenum) '=currentspike;'])
            if exist(spikefile,'file')
                eval(['save ' spikefile ' Spike_' int2str(filenum) ' -append'])
            else
                eval(['save ' spikefile ' Spike_' int2str(filenum)])
            end
            
            
            % Update the spike list
            if isempty(strmatch(sprintf('Spike_%d',filenum),spikelist,'exact'))
                spikelist=[spikelist;sprintf('Spike_%d',filenum)];
            end
            
            % Update the plot
            lbxfcn
            axis([plotlimX plotlimY])
            
        end
    case 'V-Clamp'
        IN=inpolygon(currentsweep.data.time,currentsweep.data.current*1000/currentsweep.amplifier.Igain,spikeareaX,spikeareaY);
        % If no spike is selected with rectancle, the max voltage point will be added as a new spike.
        if max(IN)>0
            selectedDATA=[currentsweep.data.time(IN),currentsweep.data.current(IN)*1000/currentsweep.amplifier.Igain];
            newspike=[selectedDATA(find(selectedDATA(:,2)==min(selectedDATA(:,2)),1,'first'),1),...
                selectedDATA(find(selectedDATA(:,2)==min(selectedDATA(:,2)),1,'first'),2)];
            currentspike=[currentspike;newspike];
            currentspike=sortrows(currentspike,1);
            % Save in the spike file
            eval(['Spike_' int2str(filenum) '=currentspike;'])
            if exist(spikefile,'file')
                eval(['save ' spikefile ' Spike_' int2str(filenum) ' -append'])
            else
                eval(['save ' spikefile ' Spike_' int2str(filenum)])
            end
            
            
            % Update the spike list
            if isempty(strmatch(sprintf('Spike_%d',filenum),spikelist,'exact'))
                spikelist=[spikelist;sprintf('Spike_%d',filenum)];
            end
            
            % Update the plot
            lbxfcn
            axis([plotlimX plotlimY])
            
        end
end
set(manualh,'enable','on')