%Code to prepare the I-clamp Rinput data to be analyzed by 'cftool'
%1.Select traces to be averaged in thview window
%2.run this code (you may have to choose correct time window
%('extractregion') in this code)
%3.Launch 'cftool' and import data (fitx, fity)
%4.Do fitting using custum equation:y=a*exp(-b*x)+c
%You may need to adjust initial values (a=15, b=40, c=-10)
%c(mV)/injected current(pA)=Rinput(Mohm)
%5.Save cftool session just in case

fity=[];
fitx=[];
tempaverage=[];
extractregion=[1,2];

if get(freeselecth,'value')==1
    multifilenum=freeselection;
end

for i=1:length(multifilenum)
    if ~rejectedsweeps(multifilenum(i))
        clear Data_*
        eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
        eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
        tempaverage(:,i)=currentsweep.data.voltage*1000/currentsweep.amplifier.Vgain;
    end
end

fity=mean(tempaverage,2);
fity=fity-mean(fity(1:extractregion(1)*currentsweep.parameter.ai_sr));
fity=fity(extractregion(1)*currentsweep.parameter.ai_sr:...
    extractregion(2)*currentsweep.parameter.ai_sr);

fitx=[0:1/currentsweep.parameter.ai_sr:extractregion(2)-extractregion(1)];
figure
plot(fitx,fity)
clear tempaverage extractregion