function [COEFF,SCORE,latent,tsquare]=PCAspikebin(SpikeCountBinCell,Select)

ALL=cell2mat(SpikeCountBinCell(Select,2));
CO=[0 0 1;...blue
    1 0 0;...red
    0 1 0;...green
    1 0 1;...magenta
    0 0 0;...black
    0 1 1;...cyan
    1 1 0];...yellow

[COEFF,SCORE,latent,tsquare]=princomp(ALL);

figure
set(gcf,'color','w')
axrow=2*(length(Select)+1);
subplot(axrow,2,1:2:axrow-1)
rownum=1;
for i=1:length(Select)
    plot(SCORE(rownum:rownum+size(SpikeCountBinCell{Select(i),2},1),1),...
        SCORE(rownum:rownum+size(SpikeCountBinCell{Select(i),2},1),2),...
        '.','markeredgecolor',CO(i,:))
    rownum=rownum+size(SpikeCountBinCell{Select(i),2},1)-1;
    hold on
    grid on
end
xlabel('PC1')
ylabel('PC2')
legend(SpikeCountBinCell(Select,1))

subplot(axrow,2,2:2:axrow)
rownum=1;
for i=1:length(Select)
    plot3(SCORE(rownum:rownum+size(SpikeCountBinCell{Select(i),2},1),1),...
        SCORE(rownum:rownum+size(SpikeCountBinCell{Select(i),2},1),2),...
        SCORE(rownum:rownum+size(SpikeCountBinCell{Select(i),2},1),3),...
        '.','markeredgecolor',CO(i,:))
    rownum=rownum+size(SpikeCountBinCell{Select(i),2},1)-1;
    hold on
    grid on
end
xlabel('PC1')
ylabel('PC2')
zlabel('PC3')
rotate3d(gca)

subplot(axrow,2,axrow+1:2:2*axrow-1)
pareto(100*latent/sum(latent))

for i=1:length(Select)
    axhand=subplot(axrow,2,axrow+2*i);
    imagesc(SpikeCountBinCell{Select(i),2},[min(min(ALL)),max(max(ALL))])
    ylabel(SpikeCountBinCell{Select(i),1})
    AXhand(i)=axhand;
    set(gca,'ytick',[],'xtick',[]);
    
end

subplot(axrow,2,2*axrow)
imagesc(COEFF(:,1:3)')
set(gca,'yticklabel',{'PC1','PC2','PC3'})
ylabel('Loadings')
xlabel('Bin#')

for i=1:length(Select)
    Inner=get(AXhand(i),'position');
    Outer=get(AXhand(i),'outerposition');
    set(AXhand(i),'position',[Inner(1),Outer(2),Inner(3),Outer(4)]);
end
