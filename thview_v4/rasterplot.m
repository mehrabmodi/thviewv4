rasterX = [];
rasterY = [];
barlength=0.8; %Must be <1
odorcolor=[1 1 .8];
shockcolor=[1 .5 .5];
sweeplength=[];
Ytick={};
odorpatchX=[];
odorpatchY=[];
shockpatchX=[];
shockpatchY=[];

if get(freeselecth,'value')==1
    multifilenum=freeselection;
end
multifilenum=multifilenum(~rejectedsweeps(multifilenum));

for i=1:length(multifilenum)
    clear Data_*
    clear Spike_*
    
    if isempty(strmatch(sprintf('Spike_%d',multifilenum(i)),spikelist,'exact'))
        errordlg('Detect spikes before creating raster plot!','Spike Not Found','modal')
        return
    end
    
    eval(['load ' loadfile ' Data_' int2str(multifilenum(i))])
    eval(['currentsweep=Data_' int2str(multifilenum(i)) ';'])
    eval(['load ' spikefile ' Spike_' int2str(multifilenum(i))])
    eval(['currentspike=Spike_' int2str(multifilenum(i)) ';'])
    
    %Trick form Vivek's code
    if ~isempty(currentspike)
        n=3*size(currentspike,1);
        x=NaN(n,1);
        y=x;
        x(1:3:n)=currentspike(:,1);
        x(2:3:n)=currentspike(:,1);
        y(1:3:n)=length(multifilenum)-i+1+barlength/2;
        y(2:3:n)=length(multifilenum)-i+1-barlength/2;
        
        rasterX=[rasterX;x];
        rasterY=[rasterY;y];
        
    end
    %To define Xlim
    sweeplength(i)=currentsweep.parameter.dur;
    %To define Ytick label
    %Ytick{length(multifilenum)-i+1,1}=sprintf('sweep%d',multifilenum(i));
    %Create patch to show odor timing
    if isfield(currentsweep,'odor') && ~strcmpi(currentsweep.odor,'control')
        odorpatchX(:,end+1)=[currentsweep.parameter.preO;...
                             currentsweep.parameter.preO+currentsweep.parameter.odorD;...
                             currentsweep.parameter.preO+currentsweep.parameter.odorD;...
                             currentsweep.parameter.preO];
        odorpatchY(:,end+1)=[length(multifilenum)-i+0.5;...
                             length(multifilenum)-i+0.5;...
                             length(multifilenum)-i+1.5;...
                             length(multifilenum)-i+1.5];
        Ytick{i}={sprintf('%s  (%d)   ',currentsweep.odor,multifilenum(i));...
                  sprintf('  %s',currentsweep.time)};
    else
        Ytick{i}={sprintf('No Odor  (%d)   ',multifilenum(i));...
                  sprintf('  %s',currentsweep.time)};
    end
    %Create patch to show shock timing
    if strcmpi(currentsweep.menuname,'Shock Only')
        currentsweep.parameter.preS=currentsweep.parameter.pre;
    end
    if (isfield(currentsweep,'shock') && currentsweep.shock) || strcmpi(currentsweep.menuname,'Shock Only')
        shockpatchX(:,end+1:end+currentsweep.parameter.shockN)...
            =[currentsweep.parameter.preS:currentsweep.parameter.isi:currentsweep.parameter.preS+currentsweep.parameter.isi*(currentsweep.parameter.shockN-1);...
            currentsweep.parameter.preS+currentsweep.parameter.shockD/1000:currentsweep.parameter.isi:currentsweep.parameter.preS+currentsweep.parameter.isi*(currentsweep.parameter.shockN-1)+currentsweep.parameter.shockD/1000;...
            currentsweep.parameter.preS+currentsweep.parameter.shockD/1000:currentsweep.parameter.isi:currentsweep.parameter.preS+currentsweep.parameter.isi*(currentsweep.parameter.shockN-1)+currentsweep.parameter.shockD/1000;...
            currentsweep.parameter.preS:currentsweep.parameter.isi:currentsweep.parameter.preS+currentsweep.parameter.isi*(currentsweep.parameter.shockN-1)];
        shockpatchY(1,end+1:end+currentsweep.parameter.shockN)=length(multifilenum)-i+0.5;
        shockpatchY(2,end-currentsweep.parameter.shockN+1:end)=length(multifilenum)-i+0.5;
        shockpatchY(3,end-currentsweep.parameter.shockN+1:end)=length(multifilenum)-i+1.5;
        shockpatchY(4,end-currentsweep.parameter.shockN+1:end)=length(multifilenum)-i+1.5;
    end
    
end

figure('position',[28 307 1225 618]);
plot(rasterX,rasterY,'k-')
set(gca,'Xlim',[0 ceil(max(sweeplength))],...
    'Ylim',[0 length(multifilenum)+1],'YTick',1:1:i,...
    'YTickLabel',[],'Tickdir','out','box','off')
if ~isempty(odorpatchX)
    patch(odorpatchX,odorpatchY,-ones(size(odorpatchX)),odorcolor,'EdgeColor','none');
end
if ~isempty(shockpatchX)
    patch(shockpatchX,shockpatchY,-0.5*ones(size(shockpatchX)),shockcolor,'EdgeColor','none');
end

%Set Yticklabels
for i=1:length(multifilenum)
    text(get(gca,'Xlim')*[1;0],length(multifilenum)-i+1,Ytick{i}(1),...
        'HorizontalAlignment','right');
    text(get(gca,'Xlim')*[0;1],length(multifilenum)-i+1,Ytick{i}(2),...
        'HorizontalAlignment','left');
end

set(freeselecth,'value',0)
freeselectmode