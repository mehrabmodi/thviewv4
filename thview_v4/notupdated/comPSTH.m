function compsth=comPSTH(varargin)

compsth=[];
for i=1:length(varargin)
    if size(varargin{i},1)>1 
        varargin{i}=varargin{i}(1:end-1,:);
    end
    
    compsth=[compsth;varargin{i}];
end

compsth(end+1,1).file={compsth(1:end,1).file};


for i=1:size(compsth,2)
    compsth(end,i).file=compsth(end,1).file;
    compsth(end,i).binsize=compsth(1,1).binsize;
    compsth(end,i).odor=compsth(1,i).odor;
    alltrace=[compsth(1:end-1,i).average];
    compsth(end,i).all=[alltrace(:,1),alltrace(:,2:2:end)];
    compsth(end,i).average=[alltrace(:,1),...time
                            mean(compsth(end,i).all(:,2:end),2),...mean
                            std(compsth(end,i).all(:,2:end),1,2),...SD
                            std(compsth(end,i).all(:,2:end),1,2)...sem
                            ./sqrt((size(compsth,1)-1))];
end


%%Now Plot!

odororder={'apricot';'apricot/-2';'apricot/-4';...
           'vinegar';'vinegar/-1';'vinegar/-2';...
           'hexanal/-2';'hexanal/-4';'hexanal/-6';...
           'isoamylacetate/-2';'isoamylacetate/-4';'isoamylacetate/-6';...
           'benzaldehyde/-2';'benzaldehyde/-4';'benzaldehyde/-6';...
           '2-heptanone/-2';'2-heptanone/-4';'2-heptanone/-6';...
           '1-octanol/-2';'1-octanol/-4';'1-octanol/-6';...
           'banana';'air'};
       
figure;
for i=1:size(compsth,2)
    subplot(8,3,strmatch(compsth(end,i).odor,odororder,'exact'))
    errorbar(compsth(end,i).average(:,1),compsth(end,i).average(:,2),...
             compsth(end,i).average(:,4))
    title(compsth(end,i).odor)
    ylim([0,20])
    xlim([0,10])
end

    