clear psthsummary
binsize=inputdlg({'Enter Bin size (msec)'});
binsize=str2num(binsize{1});
psthsummary(1).odor='tobedeleted';

for i=1:numsweep
    if ~isempty(strmatch(sprintf('Spike_%d',i),spikelist,'exact'))
        if rejectedsweeps(i)==0
            clear Data_*
            clear Spike_*
            eval(['load ' loadfile ' Data_' int2str(i) ';'])
            eval(['currentsweep=Data_' int2str(i) ';'])
            eval(['load ' spikefile ' Spike_' int2str(i) ';'])
            eval(['currentspike=Spike_' int2str(i) ';'])
            psthtime=[0:binsize/2/1000:currentsweep.parameter.dur]';
            spikerate=zeros(size(psthtime));
            
            if isempty(strmatch(currentsweep.odor,{psthsummary.odor},'exact'))
                psthsummary(end+1).odor=currentsweep.odor;
                psthsummary(end).sweep=i;
                psthsummary(end).all=psthtime;
                psthsummary(end).average=psthtime;
                
            else
                psthsummary(strmatch(currentsweep.odor,{psthsummary.odor},'exact'))...
                    .sweep(end+1)=i;
            end
            
            if ~isempty(currentspike)
            for j=1:size(psthtime,1)
                spikerate(j)=sum(currentspike(:,1)>=psthtime(j)-binsize/2/1000 ...
                               & currentspike(:,1)<psthtime(j)+binsize/2/1000)...
                                /binsize*1000;
            end
            end
            
            psthsummary(strmatch(currentsweep.odor,{psthsummary.odor},'exact'))...
                  .all(:,end+1)=spikerate;
            psthsummary(strmatch(currentsweep.odor,{psthsummary.odor},'exact'))...
                  .average(:,2)=mean(psthsummary(strmatch(currentsweep.odor,{psthsummary.odor},'exact'))...
                  .all(:,2:end),2);
        end
    end
end

psthsummary=psthsummary(2:end);
[sortedodor,sortidx]=sort({psthsummary.odor});
psthsummary=psthsummary(sortidx);
psthsummary(1).binsize=binsize;
psthsummary(1).file=FileName(1:end-4);

odororder={'apricot';'apricot/-2';'apricot/-4';...
           'vinegar';'vinegar/-1';'vinegar/-2';...
           'hexanal/-2';'hexanal/-4';'hexanal/-6';...
           'isoamylacetate/-2';'isoamylacetate/-4';'isoamylacetate/-6';...
           'benzaldehyde/-2';'benzaldehyde/-4';'benzaldehyde/-6';...
           '2-heptanone/-2';'2-heptanone/-4';'2-heptanone/-6';...
           '1-octanol/-2';'1-octanol/-4';'1-octanol/-6';...
           'banana';'air'};
       
figure;
for i=1:length(psthsummary)
    subplot(8,3,strmatch(psthsummary(i).odor,odororder,'exact'))
    plot(psthsummary(i).average(:,1),psthsummary(i).average(:,2))
    title(psthsummary(i).odor)
    ylim([0,16])
end
                            